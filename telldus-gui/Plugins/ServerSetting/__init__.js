/** Sensors **/
__setupPackage__( __extension__ );

__postInit__ = function() {
	application.allDoneLoading.connect( com.telldus.serversetting.init );
}

com.telldus.serversetting = function() {
	var view = null;

	function init() {
		view = new com.telldus.qml.view({
		});
		view.setProperty('serverSettingModel', com.telldus.controllers.myobject);
		view.load("main.qml");
		view.sizeRootObjectToView(true);
		application.configuration.addPage('Server', view, 'TelldusCenter_128.png');
	}
	return { //Public functions
		init:init
	}

}();
