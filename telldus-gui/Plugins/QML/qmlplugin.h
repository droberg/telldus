#ifndef QMLPLUGIN_H
#define QMLPLUGIN_H

#include <QScriptExtensionPlugin>

class QMLPlugin : public QScriptExtensionPlugin {
	Q_OBJECT
	Q_PLUGIN_METADATA(IID "QMLInterface" FILE "telldusqml.json")
public:
	QMLPlugin ( QObject * parent = 0 );
	~QMLPlugin ();

	virtual void initialize ( const QString & key, QScriptEngine * engine );
	virtual QStringList keys () const;

private:

};


#endif // SYSTRAYPLUGIN_H
