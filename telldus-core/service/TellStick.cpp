#include "service/Log.h"

#include "TellStick.h"

TellStickDescriptor::TellStickDescriptor()
{

}

TellStickDescriptor::~TellStickDescriptor()
{

}



TellStick::TellStick(int id, TelldusCore::EventRef event, TelldusCore::EventRef updateEvent) :
  Controller(id, event, updateEvent)
{
}

bool TellStick::haveRawCapability() const
{
	Log::error("Call of virtual haveRawCapabilities in Tellstick.cpp");
	return true;
}

std::string TellStick::serial() const
{
	Log::error("Call of virtual serial in Tellstick.cpp");
	return "";
}


bool TellStick::isOpen() const
{
	Log::error("Call of virtual isOpen in Tellstick.cpp");
	return false;
}

bool TellStick::isSameAsDescriptor(const TellStickDescriptor *d) const
{
	Log::error("Call of virtual isSameAsDescriptor in Tellstick.cpp");
	return false;
}

int TellStick::reset()
{
	Log::error("Call of virtual reset in Tellstick.cpp");
	return 0;
}

int TellStick::send( const std::string &message )
{
	Log::error("Call of virtual send in Tellstick.cpp");
	return 0;
}

bool TellStick::stillConnected() const
{
	return false;
}
