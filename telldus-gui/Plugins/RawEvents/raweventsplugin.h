#ifndef RAWEVENTSPLUGIN_H
#define RAWEVENTSPLUGIN_H

#include <QScriptExtensionPlugin>
#include <QSettings>
#include <QObject>
#include <QQmlEngine>
#include <QMutex>

#include <telldus-core.h>

// -------------------
// RawDataObject
// -------------------
class RawDataObject : public QObject{
	Q_OBJECT
	Q_PROPERTY(QString theText READ theText WRITE setText NOTIFY textChanged)
	
public:
	explicit RawDataObject (QObject* parent = 0) : QObject(parent) {
		rawDeviceEventId = tdRegisterRawDeviceEvent(reinterpret_cast<TDRawDeviceEvent>(&RawDataObject::rawData), this);
	}
	QString theText() {
		m_mutex.lock();
		QString returnText = m_stringList.join('\n');
		m_mutex.unlock();
		return returnText;
	}

	void setText(const QString &tText){
		m_mutex.lock();
		if (m_stringList.size() > 0 && tText == m_stringList[0])
		{
			m_mutex.unlock();
			return;
		}
		m_stringList.prepend(tText);
		//printf("StringListSize = %i\n",m_stringList.size());
		while(m_stringList.size() > 100 )
			m_stringList.removeAt(m_stringList.size()-1);
		m_mutex.unlock();
		emit textChanged();
	}

	Q_INVOKABLE void clearText() {
		m_mutex.lock();
		m_stringList.clear();
		m_mutex.unlock();
		emit textChanged();
	}
	Q_INVOKABLE void start() {
	connect(this, SIGNAL(rawDataReceived(const QString &, int)), this, SLOT(rawDataSlot(const QString &, int)), Qt::QueuedConnection);
}
	Q_INVOKABLE void stop() {
	disconnect(this, SIGNAL(rawDataReceived(const QString &, int)), this, SLOT(rawDataSlot(const QString &, int)));
}
private:
	QMutex m_mutex;
	int rawDeviceEventId;
	QStringList m_stringList;
	
private slots:
	void rawDataSlot( const QString &data, int controllerId ) {
		std::string ctrlId = std::to_string(controllerId);
		QString dataStr;
		QString dataInput = data;
		dataInput.replace(";","\t");
		dataStr += "<font color=";
		switch(controllerId){
		case 1:
			dataStr += "\"#AA0000\"";
			break;
		case 2:
			dataStr += "\"#00AA00\"";
			break;
		case 3:
			dataStr += "\"#0000AA\"";
			break;
		case 4:
			dataStr += "\"#AAAA00\"";
			break;
		case 5:
			dataStr += "\"#00AAAA\"";
			break;
		case 6:
			dataStr += "\"#AA00AA\"";
			break;
		case 0:
		default:
			dataStr += "\"#000000\"";
		}
		dataStr += ">";
		dataStr += "<strong>Ctrl ";
		dataStr += ctrlId.c_str();
		dataStr += "</strong></font>\t";
		dataStr += dataInput;
		dataStr += "<br>";
		setText(dataStr);
	}


signals:
	void rawDataReceived( const QString &, int controllerId );
	void textChanged();

private:
	static void WINAPI rawData(const char *data, int controllerId, int callbackId, void *context)
	{
		RawDataObject *dialog = reinterpret_cast<RawDataObject *>(context);
		if (dialog) {
			emit dialog->rawDataReceived(data, controllerId);
		}
	}
};



// -------------------
// RawEventsPlugin
// -------------------
class RawEventsPlugin : public QScriptExtensionPlugin {
	Q_OBJECT
	Q_PLUGIN_METADATA(IID "RawEventsInterface" FILE "telldusrawevents.json")
public:
	RawEventsPlugin ( QObject * parent = 0 );
	~RawEventsPlugin ();

	virtual void initialize ( const QString & key, QScriptEngine * engine );
	virtual QStringList keys () const;

};

Q_DECLARE_METATYPE(RawDataObject*)

#endif // RAWEVENTSPLUGIN_H
