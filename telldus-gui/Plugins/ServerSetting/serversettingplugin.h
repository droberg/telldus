#ifndef SERVERSETTINGPLUGIN_H
#define SERVERSETTINGPLUGIN_H

#include <QScriptExtensionPlugin>
#include <QSettings>

class MyObject : public QObject{
	 Q_OBJECT
public:
	explicit MyObject (QObject* parent = 0) : QObject(parent) {}
	Q_INVOKABLE int setServerIP(QString serverIP)
	{
		printf("Called setControllerIP with: %s\n",serverIP.toStdString().c_str());
		settings.setValue("ServerHost", serverIP);
		return 1;
	}
	Q_INVOKABLE QString getServerIP()
	{
		QString serverIP = settings.value("ServerHost", "127.0.0.1").toString();
		return serverIP;
	}
private:
	QSettings settings;
};


class ServersettingPlugin : public QScriptExtensionPlugin {
	Q_OBJECT
	Q_PLUGIN_METADATA(IID "ServersettingInterface" FILE "telldusserversetting.json")
public:
	ServersettingPlugin ( QObject * parent = 0 );
	~ServersettingPlugin ();

	virtual void initialize ( const QString & key, QScriptEngine * engine );
	virtual QStringList keys () const;

};

Q_DECLARE_METATYPE(MyObject*)

#endif // SERVERSETTINGPLUGIN_H
