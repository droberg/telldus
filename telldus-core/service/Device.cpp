//
// Copyright (C) 2012 Telldus Technologies AB. All rights reserved.
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "service/Device.h"
#include <list>
#include <string>
#include <sstream>
#include "common/Message.h"
#include "common/Strings.h"
#include "service/Settings.h"
#include "service/TellStickSerial.h"


class ControllerStats
{
public:
	int64_t& operator[](int a)
	{
		if( controllerIdCount.find(a) == controllerIdCount.end() )
			controllerIdCount[a] = 0;
		return controllerIdCount[a];
	};
	std::string GetControllerStats();
	void setControllerCountForId(std::string token);
	void setControllerStats(const std::string &controllerStatsString);

private:
	std::map<int,int64_t> controllerIdCount;
};

class Device::PrivateData {
public:
	std::wstring model;
	std::wstring name;
	ParameterMap parameterList;
	ControllerStats controllerIdCount;
	Protocol *protocol;
	std::wstring protocolName;
	int preferredControllerId;
	int state;
	std::wstring stateValue;
};

Device::Device(int id)
	:Mutex() {
	d = new PrivateData;
	d->protocol = 0;
	d->preferredControllerId = 0;
	d->state = 0;
}

Device::~Device(void) {
	delete d->protocol;
	delete d;
}

/**
* Get-/Set-methods
*/

int Device::getLastSentCommand(int methodsSupported) {
	int lastSentCommand = Device::maskUnsupportedMethods(d->state, methodsSupported);

	if (lastSentCommand == TELLSTICK_BELL) {
		// Bell is not a state
		lastSentCommand = TELLSTICK_TURNOFF;
	}
	if (lastSentCommand == 0) {
		lastSentCommand = TELLSTICK_UNDEFINED;
	}
	return lastSentCommand;
}

int Device::getState()
{
	return d->state;
}

int Device::getMethods() const {
	Protocol *p = this->retrieveProtocol();
	if (p) {
		return p->methods();
	}
	return 0;
}

bool Device::setLastSentCommand(int command, std::wstring value, int controllerId) {
	d->controllerIdCount[controllerId] += 1;
	if(d->state == command && 
	   ( d->stateValue == value || 
	     ( value == L"" && d->stateValue == L"0" ) || 
	     ( value == L"0" && d->stateValue == L"" ) ))
		return false;
	d->state = command;
	d->stateValue = value;
	return true;
}

std::wstring Device::getModel() {
	return d->model;
}

void Device::setModel(const std::wstring &model) {
	if(d->protocol) {
		delete(d->protocol);
		d->protocol = 0;
	}
	d->model = model;
}

std::wstring Device::getName() {
	return d->name;
}

void Device::setName(const std::wstring &name) {
	d->name = name;
}

std::wstring Device::getParameter(const std::wstring &key) {
	ParameterMap::iterator it = d->parameterList.find(key);
	if (it == d->parameterList.end()) {
		return L"";
	}
	return d->parameterList[key];
}

std::list<std::string> Device::getParametersForProtocol() const {
	return Protocol::getParametersForProtocol(getProtocolName());
}

void Device::setParameter(const std::wstring &key, const std::wstring &value) {
	d->parameterList[key] = value;
	if(d->protocol) {
		d->protocol->setParameters(d->parameterList);
	}
}

int Device::getPreferredControllerId() {
	return d->preferredControllerId;
}

void Device::setPreferredControllerId(int controllerId) {
	d->preferredControllerId = controllerId;
}

std::wstring Device::getProtocolName() const {
	return d->protocolName;
}

void Device::setProtocolName(const std::wstring &protocolName) {
	if(d->protocol) {
		delete(d->protocol);
		d->protocol = 0;
	}
	d->protocolName = protocolName;
}

std::wstring Device::getStateValue() {
	return d->stateValue;
}

int Device::getType() {
	if(d->protocolName == L"group") {
		return TELLSTICK_TYPE_GROUP;
	} else if(d->protocolName == L"scene") {
		return TELLSTICK_TYPE_SCENE;
	} else if( getParameter(L"devices") != L"" )
		return TELLSTICK_TYPE_DEVICEGROUP;
	return TELLSTICK_TYPE_DEVICE;
}

void Device::setControllerStats(std::string controllerStatsString)
{
	d->controllerIdCount.setControllerStats(controllerStatsString);
}

std::string Device::getControllerStats()
{
	return d->controllerIdCount.GetControllerStats();
}

/**
* End Get-/Set
*/

int Device::doAction(int action, unsigned char data, Controller *controller) {
	Protocol *p = this->retrieveProtocol();
	if (!p) {
		// Syntax error in configuration, no such protocol
		return TELLSTICK_ERROR_CONFIG_SYNTAX;
	}
	// Try to determine if we need to call another method due to masking
	int method = this->isMethodSupported(action);
	if (method <= 0) {
		return TELLSTICK_ERROR_METHOD_NOT_SUPPORTED;
	}
	if(controller->haveRawCapability())
	{
		std::string code = p->getStringForMethod(method, data, controller);
		if (code == "") {
			return TELLSTICK_ERROR_METHOD_NOT_SUPPORTED;
		}
		if (code[0] != 'S' && code[0] != 'T' && code[0] != 'P' && code[0] != 'R') {
			// Try autodetect sendtype
			TellStickSerial *tellstick = dynamic_cast<TellStickSerial *>(controller);
			unsigned int maxlength = 512;
			if (tellstick && tellstick->pid() != 0x0c31)
			{
				maxlength = 80;
			}
			if (code.length() <= maxlength)
			{
				// S is enough
				code.insert(0, 1, 'S');
				code.append(1, '+');
			} else {
				code = TellStickSerial::createTPacket(code);
			}
		}
		return controller->send(code);
	}
	else
	{
		std::string protocolName = TelldusCore::wideToString(d->protocolName);
		std::string model = TelldusCore::wideToString(d->model);
		std::string code = "4:sendh8:protocol" +TelldusCore::intToHexString(protocolName.length()) + ":" + protocolName + "5:model" + TelldusCore::intToHexString(model.length()) + ":" + model;
		std::list<std::string> paramList = getParametersForProtocol();
		for (std::list<std::string>::iterator it = paramList.begin(); it != paramList.end(); ++it){
			std::wstring key = TelldusCore::charToWstring(it->c_str());
			std::string parameter = TelldusCore::wideToString(getParameter(key));
			try
			{
				int i_dec = std::stoi(parameter,nullptr,10);
				parameter = "";
				if (i_dec < 0)
				{
					parameter += "-";
				}
				parameter += TelldusCore::intToHexString(i_dec, *it == "unit");
				parameter = "i" + parameter + "s";
			}
			catch (std::invalid_argument)
			{
				parameter = TelldusCore::intToHexString(parameter.length()) + ":" + parameter;
			}
			code +=  TelldusCore::intToHexString(it->length()) + ":" + *it + parameter;
		}
		code += "6:methodi" +  TelldusCore::intToHexString(method) + "ss";
		return controller->send(code);
	}
	return TELLSTICK_SUCCESS;
}

int Device::isMethodSupported(int method) const {
	Protocol *p = this->retrieveProtocol();
	if (!p) {
		// Syntax error in configuration, no such protocol
		return TELLSTICK_ERROR_CONFIG_SYNTAX;
	}
	// Try to determine if we need to call another method due to masking
	int methods = p->methods();
	if ((method & methods) == 0) {
		// Loop all methods an see if any method masks to this one
		for(int i = 1; i <= methods; i <<= 1) {
			if ((i & methods) == 0) {
				continue;
			}
			if (this->maskUnsupportedMethods(i, method)) {
				method = i;
				break;
			}
		}
	}
	if ((method & methods) == 0) {
		return TELLSTICK_ERROR_METHOD_NOT_SUPPORTED;
	}
	return method;
}

Protocol* Device::retrieveProtocol() const {
	if (d->protocol) {
		return d->protocol;
	}

	d->protocol = Protocol::getProtocolInstance(d->protocolName);
	if(d->protocol) {
		d->protocol->setModel(d->model);
		d->protocol->setParameters(d->parameterList);
		return d->protocol;
	}

	return 0;
}

int Device::maskUnsupportedMethods(int methods, int supportedMethods) {
	// Bell -> On
	if ((methods & TELLSTICK_BELL) && !(supportedMethods & TELLSTICK_BELL)) {
		methods |= TELLSTICK_TURNON;
	}

	// Execute -> On
	if ((methods & TELLSTICK_EXECUTE) && !(supportedMethods & TELLSTICK_EXECUTE)) {
		methods |= TELLSTICK_TURNON;
	}

	// Up -> Off
	if ((methods & TELLSTICK_UP) && !(supportedMethods & TELLSTICK_UP)) {
		methods |= TELLSTICK_TURNOFF;
	}

	// Down -> On
	if ((methods & TELLSTICK_DOWN) && !(supportedMethods & TELLSTICK_DOWN)) {
		methods |= TELLSTICK_TURNON;
	}

	// Cut of the rest of the unsupported methods we don't have a fallback for
	return methods & supportedMethods;
}

int Device::methodId( const std::string &methodName ) {
	if (methodName.compare("turnon") == 0) {
		return TELLSTICK_TURNON;
	}
	if (methodName.compare("turnoff") == 0) {
		return TELLSTICK_TURNOFF;
	}
	if (methodName.compare("bell") == 0) {
		return TELLSTICK_BELL;
	}
	if (methodName.compare("dim") == 0) {
		return TELLSTICK_DIM;
	}
	if (methodName.compare("execute") == 0) {
		return TELLSTICK_EXECUTE;
	}
	if (methodName.compare("up") == 0) {
		return TELLSTICK_UP;
	}
	if (methodName.compare("down") == 0) {
		return TELLSTICK_DOWN;
	}
	if (methodName.compare("stop") == 0) {
		return TELLSTICK_STOP;
	}
	return 0;
}

std::string ControllerStats::GetControllerStats()
{
	std::stringstream ss;
	std::map<int,int64_t>::iterator it;

	for (it = controllerIdCount.begin(); it != controllerIdCount.end(); it++)
	{
		if(it->first >= 0 && it->second > 0) {
		ss << it->first
		   << ':'
		   << it->second
		   << ';';
		}
	}
	return ss.str();
}

void ControllerStats::setControllerCountForId(std::string token)
{
	size_t pos =token.find(":");
	int64_t controllerId = std::stoi(token.substr(0, pos),nullptr);
	int64_t value = std::stoi(token.erase(0, pos + 1),nullptr);
	controllerIdCount[controllerId] = value;
}

void ControllerStats::setControllerStats(const std::string &controllerStatsString)
{
	controllerIdCount.clear();
	std::string localStr = controllerStatsString;
	size_t pos = 0;
	std::string token;
	while ((pos = localStr.find(";")) != std::string::npos) {
		token = localStr.substr(0, pos);
		setControllerCountForId(token);
		localStr.erase(0, pos + 1);
	}
	if(localStr.length() > 0)
		token = localStr.substr(0, pos);
	return;
}
