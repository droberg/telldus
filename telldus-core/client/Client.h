//
// Copyright (C) 2012 Telldus Technologies AB. All rights reserved.
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef TELLDUS_CORE_CLIENT_CLIENT_H_
#define TELLDUS_CORE_CLIENT_CLIENT_H_

#ifdef _WINDOWS
#include <winsock2.h>
#include <Ws2tcpip.h>
#else
#include <netinet/in.h>
#include <arpa/inet.h>
#endif

#include "client/telldus-core.h"
#include "client/CallbackDispatcher.h"
#include "common/Message.h"
#include "common/Thread.h"

namespace TelldusCore {
	class Client : public Thread {
	public:
		~Client(void);

		static Client *getInstance(bool runInstance = true);
		static Client *getInstance(std::string serverAddress, int clientPort, int eventPort,bool runInstance = true);
		static void close();

		int registerEvent(CallbackStruct::CallbackType type, void *eventFunction, void *context );
		void stopThread(void);
		int unregisterCallback( int callbackId );
		int getSensor(char *protocol, int protocolLen, char *model, int modelLen, char *name, int nameLen, int *id, int *dataTypes);
		int getController(int *controllerId, int *controllerType, char *name, int nameLen, int *available);

		static bool getBoolFromService(const Message &msg);
		static int getIntegerFromService(const Message &msg);
		static std::wstring getWStringFromService(const Message &msg);

	protected:
			void run(void);

	private:
		Client();
		Client(std::string serverAddress, int clientPort, int eventPort);
		static std::wstring sendToService(const Message &msg);
		std::wstring internalSendToService(const Message &msg);
		static int getServerIpFromHostName(sockaddr_in *remoteServerAddr,std::string hostName, int port);

		class PrivateData;
		PrivateData *d;
		static Client *instance;
	};
}

#endif  // TELLDUS_CORE_CLIENT_CLIENT_H_
