//
// Copyright (C) 2012 Telldus Technologies AB. All rights reserved.
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef TELLDUS_CORE_SERVICE_TELLSTICKSERIAL_H_
#define TELLDUS_CORE_SERVICE_TELLSTICKSERIAL_H_

#include <list>
#include <string>
#include "service/Controller.h"
#include "common/Thread.h"
#include "TellStick.h"

class TellStickSerialDescriptor : public TellStickDescriptor {
public:
	virtual ~TellStickSerialDescriptor();
	int vid, pid;
};

class TellStickSerial : public TellStick {
public:
	TellStickSerial(int controllerId, TelldusCore::EventRef event, TelldusCore::EventRef updateEvent, const TellStickDescriptor *td);
	virtual ~TellStickSerial();

	virtual int pid() const;
	virtual int vid() const;
	virtual std::string serial() const;

	virtual bool haveRawCapability() const {return true;};
	virtual bool isOpen() const;
	virtual bool isSameAsDescriptor(const TellStickDescriptor *td) const;
	virtual int reset();
	virtual int send( const std::string &message );
	virtual bool stillConnected() const;

	static std::list<TellStickSerialDescriptor> findAll();
	static std::string convertSToT(  unsigned char t0, unsigned char t1, unsigned char t2, unsigned char t3, const std::string &data );
	static std::string createTPacket( const std::string & );

protected:

	void processData( const std::string &data );
	void run();
	void setBaud( int baud );
	void stop();

private:
	static std::list<TellStickSerialDescriptor> findAllByVIDPID( int vid, int pid );

	class PrivateData;
	PrivateData *d;
};

#endif  // TELLDUS_CORE_SERVICE_TELLSTICKSERIAL_H_
