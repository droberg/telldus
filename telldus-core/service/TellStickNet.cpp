//
// Copyright (C) 2012 Telldus Technologies AB. All rights reserved.
//
// Copyright: See COPYING file that comes with this distribution
//
//

// USING THIS FOR DOCUMENTATION:
// http://developer.telldus.com/doxygen/TellStickNet.html
// http://developer.telldus.com/blog/2012/03/02/help-us-develop-local-access-using-tellstick-net-build-your-own-firmware
// http://developer.telldus.com/ticket/114
// From : tellsticknet.c handleMessage()
// registerListener()
// send()
// setIp()
// saveIp()
// disconnect()

#include <sstream>
#include <iomanip>
#include <iostream>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <locale>
#include <codecvt>
#include <string>
#ifndef  _WINDOWS
#include <unistd.h>
#endif
#include <list>
#include <map>
#include <string>
#include <chrono>
#include <sys/types.h>

#include "common/WinSocketHandler.h"

#include "service/TellStickNet.h"
#include "service/Log.h"
#include "service/Protocol.h"
#include "service/Settings.h"
#include "client/telldus-core.h"
#include "common/Thread.h"
#include "common/Message.h"
#include "common/Mutex.h"
#include "common/Strings.h"
#include "common/common.h"

#define BROADCAST_ADDRESS  ( "255.255.255.255" )
#define BROADCAST_PORT     ( 30303 )
#define BROADCAST_TOKEN    ( 'D' )

#define TELLSTICK_NET_PORT ( 42314 )
#define BUFER_MAXSIZE      ( 1024 )

TellStickNetDescriptor::~TellStickNetDescriptor()
{

}

std::string TellStickNet::takeString(std::string *message) {
	if (message->size() <= 0) {
		return "";
	}
	int index = message->find(':');
	if(index >= 0)
	{
		message->substr(0, index);
		std::string retval(message->substr(0, index));
		message->erase(0, index+1);
		return retval;
	}
	else
	{
		std::string retval(*message);
		*message = "";
		return retval;
	}
}

class TellStickNet::PrivateData {
public:
	bool open, ignoreControllerConfirmation;
	bool haveRawCapabilies;
	std::string ip, product, mac_address, activation_code, message;
	int udpSocket;
#ifdef _WINDOWS
	HANDLE eh;
#else
// #include <unistd.h>
	struct {
		pthread_cond_t eCondVar;
		pthread_mutex_t eMutex;
	} eh;
#endif
	bool running;
	TelldusCore::Mutex mutex;
};



TellStickNet::TellStickNet(int controllerId, TelldusCore::EventRef event, TelldusCore::EventRef updateEvent, const TellStickDescriptor *td )
  :TellStick(controllerId, event, updateEvent)
{
	initSocketsIfNecessary();
	const TellStickNetDescriptor *tdPtr = dynamic_cast<const TellStickNetDescriptor *>( td );
	d = new PrivateData;
	d->open = false;
	d->running = false;
	d->udpSocket = -1;
	if(!(tdPtr == nullptr))
	{
		d->ip = tdPtr->ip;
		d->mac_address = tdPtr->mac_address;
		d->activation_code = tdPtr->serial;
		d->haveRawCapabilies = tdPtr->haveRawCapabilities;
	}
	else
	{
		d->ip = "Unknown";
		d->mac_address = "Unknown";
		d->activation_code = "Unknown";
		d->haveRawCapabilies = false;
		Log::error("TellStickNet constructor called with wrong descriptor type. This is a programming error.");
		return;
	}

	Settings set;
	d->ignoreControllerConfirmation = set.getSetting(L"ignoreControllerConfirmation") == L"true";

	Log::notice("Connecting to TellStick on: %s (with mac / serial: %s / %s)", d->ip.c_str(), d->mac_address.c_str(), d->activation_code.c_str());
	if ( setupConnection() < 0)
	{
		Log::error("Failed to open TellStick");
		return;
	}

	// There is no info in the return value, so we must assume all is well
	d->open = true;
	this->start();
}

TellStickNet::~TellStickNet() {
	Log::notice("Disconnecting TellStick on: %s (with mac / serial: %s / %s)", d->ip.c_str(), d->mac_address.c_str(), d->activation_code.c_str());
	if (d->running) {
		stop();
	}
	if(d->udpSocket != -1)
		SOCKCLOSE(d->udpSocket);
	delete d;
}

bool TellStickNet::haveRawCapability() const
{
	return d->haveRawCapabilies;
}

std::string TellStickNet::ip() const {
	return d->ip;
}

std::string TellStickNet::mac() const {
	return d->mac_address;
}

std::string TellStickNet::serial() const
{
	return d->activation_code;
}

// This is mainly used after loadcontrollers to see whether we succeeded
bool TellStickNet::isOpen() const {
	return d->open;
}

bool TellStickNet::isSameAsDescriptor(const TellStickDescriptor *td) const {
	const TellStickNetDescriptor* tdPtr = dynamic_cast<const TellStickNetDescriptor*>(td);
	if( tdPtr == nullptr )
	{
		return false;
	}
	if (td->serial != d->activation_code) {
		return false;
	}
	return true;
}

std::string netToSerialTranslate(std::string &message)
{
	if(message.size() == 0)
		return "";
	std::string outStr = "";
	switch (message.at(0)) {
		case 'h' :
		{
			// Dictionary, replace with key:value;key:value
			// h3:foo3:bar5:hello5:worlds will be foo:bar;hello:world;
			message.erase(0, 1); // Remove h
			while (message.at(0) != 's')
			{
				std::string key = TelldusCore::Message::takeString(&message);
				outStr += key + ":" ;
				std::string value = netToSerialTranslate(message);
				outStr += value + ";" ;
			}
			message.erase(0, 1); // Remove finishing s
		}
			break;
		case 'l' :
		{
			// List, replace with key:value (ex : l3:bar3:bazs => bar:baz)
			// (So in total we have h3:fool3:bar3:bazss corresponds to foo:bar:baz; )
			// I know this is not a perfect match..
			message.erase(0, 1); // Remove l
			while (message.at(0) != 's')
			{
				outStr += netToSerialTranslate(message) + ":" ;
			}
			message.erase(0, 1); // Remove finishing s
		}
			break;
		case 'i' :
			outStr += "0x" +  TelldusCore::Message::takeIntAsString(&message);
			// Integer
			break;
		case 's' :
			message.erase(0, 1); // Remove s
		default :
			// Hopefully a string
			outStr += TelldusCore::Message::takeString(&message);
			break;
	}
	return outStr;
}

void TellStickNet::processData( const std::string &data ) {
	// Data to parser should be on the format
	// key:value;key:value
	// We dont need this for NET messages, assuming they are complete ...
	d->message.clear();
	d->message = data;
	//this->publishData(d->message.substr(2));
	std::string cmd = TelldusCore::Message::takeString(&d->message);
	if(cmd == "RawData")
	{
		std::string outStr = netToSerialTranslate(d->message);
		this->decodePublishData(outStr);
	}
}

int TellStickNet::reset() {
	TelldusCore::MutexLocker locker(&d->mutex);
	if(stillConnected())
		return setupConnection();
	else
		return TELLSTICK_ERROR_NOT_FOUND;
}

int TellStickNet::setupConnection()
{
	initSocketsIfNecessary();
	// Creating socket file descriptor
	if (d->udpSocket > 0)
		SOCKCLOSE(d->udpSocket);
	if ( (d->udpSocket = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)) < 0 )
	{
		Log::error("Cannot create socket for TellStick (%s)", d->ip.c_str());
		return TELLSTICK_ERROR_UNKNOWN;
	}
	unsigned long mode = 1; // Non blocking
	int res = 0;
#ifdef _WINDOWS
	res = ioctlsocket(d->udpSocket, FIONBIO, &mode);
#else
	res = ioctl(d->udpSocket, FIONBIO, &mode);
#endif
	if( res != NO_ERROR )
	{
		Log::error("Cannot set socket to non blocking for TellStick (%s)", d->ip.c_str());
		cleanupSocketsIfNecessary();
		return TELLSTICK_ERROR_UNKNOWN;
	}
	struct sockaddr_in servaddr;
	memset(&servaddr, 0, sizeof(servaddr));
	servaddr.sin_family = AF_INET;
	servaddr.sin_port = htons(TELLSTICK_NET_PORT);
	servaddr.sin_addr.s_addr = INADDR_ANY;
#ifdef _WINDOWS
	std::wstring_convert<std::codecvt_utf8_utf16<wchar_t>> converter;
	std::wstring wideIp = converter.from_bytes( d->ip );
	if ( InetPton(AF_INET, wideIp.c_str() , &servaddr.sin_addr) == 0)
#else
	if ( inet_aton( d->ip.c_str() , &servaddr.sin_addr) == 0 )
#endif
	{
		Log::error("Failed to create valid ip using inet_aton(%s) for tellstick", d->ip.c_str());
		return TELLSTICK_ERROR_UNKNOWN;
	}
	const char *cmdString = "B:reglistener";
	if (sendto(d->udpSocket, (const char *)cmdString, strlen(cmdString),
	           0, (const struct sockaddr *) &servaddr,
	           sizeof(servaddr))==-1)
	{
		Log::error("Failed to register to tellstick controller (%s)", d->ip.c_str());
		return TELLSTICK_ERROR_UNKNOWN;
	}
	return TELLSTICK_SUCCESS;
}

void TellStickNet::run()
{
	int64_t lastSeenTimeS = -1;

	int dwBytesRead = 0;
	char buf[1024];     // = 0;

#ifdef _WINDOWS
	d->eh = CreateEvent( NULL, false, false, NULL );
#else
	pthread_mutex_init(&d->eh.eMutex, NULL);
	pthread_cond_init(&d->eh.eCondVar, NULL);
#endif
	{
		TelldusCore::MutexLocker locker(&d->mutex);
		d->running = true;
	}

	struct sockaddr_in servaddr;
	socklen_t len = sizeof(servaddr);

	memset(&servaddr, 0, sizeof(servaddr));
	servaddr.sin_family = AF_INET;
	servaddr.sin_port = htons(TELLSTICK_NET_PORT);
	servaddr.sin_addr.s_addr = INADDR_ANY;
#ifdef _WINDOWS
	std::wstring_convert<std::codecvt_utf8_utf16<wchar_t>> converter;
	std::wstring wideIp = converter.from_bytes( d->ip );
	if ( InetPton(AF_INET, wideIp.c_str() , &servaddr.sin_addr) == 0)
#else
	if ( inet_aton( d->ip.c_str() , &servaddr.sin_addr) == 0 )
#endif
	{
		Log::error("Failed to create valid ip using inet_aton(%s) for tellstick", d->ip.c_str());
		return;
	}

	while(1) {
		msleep(100);
		TelldusCore::MutexLocker locker(&d->mutex);
		if (!d->running)
		{
			break;
		}
		memset(buf, 0, sizeof(buf));
		dwBytesRead = recvfrom(d->udpSocket,buf, sizeof(buf), 0, (struct sockaddr *) &servaddr,&len);
		if (dwBytesRead < 1)
		{
			// Could be < 0 which would indicate an error, e.g. EAGAIN or just zero bytes.
			int64_t nowTimeS = std::chrono::duration_cast<std::chrono::seconds>(std::chrono::steady_clock::now().time_since_epoch()).count();
			if(nowTimeS - lastSeenTimeS > 10)
			{
				if(stillConnected())
				{
					// Device responds on the ip stored, so it should be fine to re-register as listener, and we have seen it
					lastSeenTimeS = std::chrono::duration_cast<std::chrono::seconds>(std::chrono::steady_clock::now().time_since_epoch()).count();
					if(setupConnection() < 0)
					{
						// Whatever good this does (since it's only checked at creation)
						d->open = false;
						msleep(1000); // Sleep for a second and hope that we recover, else eventually the janitor will react
					}
				}
				else
				{
					// Whatever good this does (since it's only checked at creation)
					d->open = false;
					msleep(1000); // Sleep for a second and hope that we recover, else eventually the janitor will react
				}
			}
			continue;
		}
		lastSeenTimeS = std::chrono::duration_cast<std::chrono::seconds>(std::chrono::steady_clock::now().time_since_epoch()).count();
		processData( reinterpret_cast<char *>(&buf) );
	}
}

std::string convertTToS( const std::string &data ) {
	std::string tmpStr(data);
	unsigned char t0;
	unsigned char t1;
	unsigned char t2;
	unsigned char t3;
	// Assume data has beens stripped of "S" and "+"
	t0 = tmpStr.at(0);
	t1 = tmpStr.at(1);
	t2 = tmpStr.at(2);
	t3 = tmpStr.at(3);
	unsigned char length = tmpStr.at(4);
	Log::debug("convertTToS: times are: %i,%i,%i,%i , length= %i",t0,t1,t2,t3,length);
	std::string outSring;
	for (int ii = 0; ii < (length + 3) /4; ii++)
	{
		char dataByte = tmpStr.at(5 + ii );
		for(int jj = 0; jj < 4 ; jj++)
		{
			switch(dataByte & 0xC0)
			{
				case 0x00:
					outSring += std::string(1, t0  );
					break;
				case 0x40:
					outSring += std::string(1, t1  );
					break;
				case 0x80:
					outSring += std::string(1, t2  );
					break;
				case 0xD0:
					outSring += std::string(1, t3  );
					break;
			}
			dataByte <<= 2;
		}
	}
	Log::debug("convertTToS:  out string is %i",outSring.size());
	return outSring.substr(0,length);
}


int TellStickNet::send( const std::string &strMessage ) {
	// This lock does two things
	//  1 Prevents two calls from different threads to this function
	//  2 Prevents our running thread from receiving the data we are interested in here
	TelldusCore::MutexLocker locker(&d->mutex);
	std::string finalStr;
	if(strMessage.find("4:sendh") != 0)
	{
		std::string tempString(strMessage);
		finalStr = "4:sendh1:S";
		std::string endStr = "s";
		//The parameters are sent encoded in a dictionary, with the RF-data in the key 'S'. Example sending Arctech Code switch A1 ON:
		//{'S': '$k$k$k$k$k$k$k$k$k$k$k$k$k$k$k$k$k$k$kk$$kk$$kk$$k'} The string sent will be encoded like this:
		//4:sendh1:S32:$k$k$k$k$k$k$k$k$k$k$k$k$k$k$k$k$k$k$kk$$kk$$k$k$ks
		// Incoming would be something like Txxxxx+, "Sxxxx+", "N+"
		// We have possibly prefix  R , P, D (Skip this)
		bool haveR = false, haveP = false, haveD = false;
		int valR = 0, valP = 0, valD = 0;
		while(tempString.size() > 0 && tempString.at(0) != 'S' && tempString.at(0) != 'T')
		{
			std::stringstream stream;
			switch ( tempString.at(0) )
			{
				case 'R':
					haveR = true;
					valR = tempString.at(1);
					stream << "1:Ri" <<  std::hex << valR << "s";
					endStr = stream.str()+ endStr;
					tempString.erase(0,2);
					break;
				case 'P':
					haveP = true;
					valP = tempString.at(1);
					stream << "1:Pi" <<  std::hex << valP << "s";
					endStr = stream.str()+ endStr;
					tempString.erase(0,2);
					break;
				case 'D':
					haveD = true;
					valD = tempString.at(1);
					tempString.erase(0,2);
					break;
				case 'N':
					// Check if device responds to
					return stillConnected() ? TELLSTICK_SUCCESS : TELLSTICK_ERROR_BROKEN_PIPE;
				default:
					tempString.clear();
					break;
			}
		}
		if(tempString.at(0) == 'T' )
		{
			tempString.erase(0,1);
			tempString = convertTToS(tempString);
		}
		else
			tempString.erase(0,1);
		std::stringstream stream;
		stream << std::hex << tempString.size();
		finalStr = finalStr  + stream.str() + ":"+ tempString + endStr;
	}
	else
	{
		finalStr = strMessage;
	}
	if (!d->open) {
		return TELLSTICK_ERROR_NOT_FOUND;
	}

	bool c = true;
	char *tempMessage = new char[finalStr.size()];
	memcpy(tempMessage, finalStr.c_str(), finalStr.size());

	int ret = 0;
	Log::debug("Would write %s, to %s",finalStr.c_str() ,d->ip.c_str());
	char *hexString = new char[ finalStr.length() * 2 + 1 ];
	hexString[ finalStr.length() * 2 ] = 0;
	for (unsigned int ii = 0; ii < finalStr.length(); ii++ )
	{
		char cChar = finalStr[ii];
		sprintf(&hexString[ ii * 2 ],"%x",cChar);
	}
	Log::debug("> %s",hexString);
	delete[] hexString;

	struct sockaddr_in servaddr;
	memset(&servaddr, 0, sizeof(servaddr));
	servaddr.sin_family = AF_INET;
	servaddr.sin_port = htons(TELLSTICK_NET_PORT);
	servaddr.sin_addr.s_addr = INADDR_ANY;
#ifdef _WINDOWS
	std::wstring_convert<std::codecvt_utf8_utf16<wchar_t>> converter;
	std::wstring wideIp = converter.from_bytes( d->ip );
	if ( InetPton(AF_INET, wideIp.c_str() , &servaddr.sin_addr) == 0)
#else
	if ( inet_aton( d->ip.c_str() , &servaddr.sin_addr) == 0 )
#endif
	{
		Log::error("Failed to create valid ip using inet_aton(%s) for tellstick", d->ip.c_str());
		return TELLSTICK_ERROR_NETWORK_FAIL;
	}
	ret = sendto(d->udpSocket, tempMessage, finalStr.size(),
	             0, (const struct sockaddr *) &servaddr,
	             sizeof(servaddr));
	if (ret ==-1)
	{
		Log::error("Failed to send to tellstick controller (%s)", d->ip.c_str());
		return TELLSTICK_ERROR_NETWORK_FAIL;
	}

	if(ret < 0) {
		c = false;
	} else if(ret != finalStr.length()) {
		Log::debug("Weird send length? retval %i instead of %d", ret, static_cast<int>(strMessage.length()));
	}

	delete[] tempMessage;

	if(!c) {
		Log::error("Broken pipe on send");
		return TELLSTICK_ERROR_BROKEN_PIPE;
	}

	if(d->ignoreControllerConfirmation) {
		// allow TellStick to finish its air-sending
		msleep(20);
		return TELLSTICK_SUCCESS;
	}

	return TELLSTICK_SUCCESS;
}


bool TellStickNet::stillConnected() const {
	// Some read error must have happened, or the janitor is just checking up on us
	// We should check here whether we are still connected
	bool found = false;
	std::list<TellStickNetDescriptor> tellSticksFound = findOneOrAll(d->ip);
	for(std::list<TellStickNetDescriptor>::const_iterator it = tellSticksFound.begin();
	    it != tellSticksFound.end(); ++it)
	{
		if ( it->serial == d->activation_code && it->ip == d->ip)
			found = true; // Even if the serial is the same we need reset if the ip has changed
	}
	//Log::debug("Check if we are still connected to %s returns: %i",d->ip.c_str(), found ? 1 : 0);
	return found;
}

std::list<TellStickNetDescriptor> TellStickNet::findOneOrAll(std::string toIP)
{
	initSocketsIfNecessary();
	// This function should do a broadcast and look for all tellsticks on the network
	std::list<TellStickNetDescriptor> retval;
	char buffer[BUFER_MAXSIZE];
	int sockfd;
	struct sockaddr_in servaddr;
	int n;
	socklen_t len;
	setsockoptType broadcastEnable = (toIP.size() == 0) ? 1 : 0;
	if (toIP.size() == 0)
		toIP = std::string(BROADCAST_ADDRESS);

	// Creating socket file descriptor
	if ( (sockfd = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)) < 0 ) {
		Log::error("Failed to create socket in findOneOrAll");
		return retval;
	}
	int retVal = setsockopt(sockfd, SOL_SOCKET, SO_BROADCAST, &broadcastEnable, sizeof(broadcastEnable));
	if(retVal < 0 )
	{
		Log::error("Failed to setsockopt ");
		Log::error(strerror(errno));
	}
	unsigned long mode = 1; // Non blocking
	int res = 0;
#ifdef _WINDOWS
	res = ioctlsocket(sockfd, FIONBIO, &mode);
#else
	res = ioctl(sockfd, FIONBIO, &mode);
#endif
	if (res != NO_ERROR)
	{
		Log::error("Cannot set socket to non blocking for broadcast");
		cleanupSocketsIfNecessary();
		return retval;
	}
	// Prepare sending
	memset(&servaddr, 0, sizeof(servaddr));
	servaddr.sin_family = AF_INET;
	servaddr.sin_port = htons(BROADCAST_PORT);
	servaddr.sin_addr.s_addr = INADDR_ANY;
#ifdef _WINDOWS
	std::wstring_convert<std::codecvt_utf8_utf16<wchar_t>> converter;
	std::wstring wideIp = converter.from_bytes(toIP );
	if ( InetPton(AF_INET, wideIp.c_str() , &servaddr.sin_addr) == 0)
#else
	if ( inet_aton(toIP.c_str() , &servaddr.sin_addr) == 0 )
#endif
	{
		Log::error("inet_aton() failed");
		return retval;
	}
	char outToken[1] = {BROADCAST_TOKEN};
	if (sendto(sockfd, (const char *)outToken, sizeof (outToken),
	           0, (const struct sockaddr *) &servaddr,
	           sizeof(servaddr))==-1)
	{
		Log::error("Failed to send discovery msg");
		Log::error(strerror(errno));
	}
	Log::debug("Send broadcast");
	for(int ii = 0; ii < 10 ; ii++)
	{
		msleep(40); // Significantly less than 100 / 2 = 50 msec to be safe
		len = sizeof(servaddr);
		Log::debug("Reading / waiting ");
		n = recvfrom(sockfd, (char *)buffer, BUFER_MAXSIZE,
		             0, (struct sockaddr *) &servaddr,
		             &len);
		if(n > 0)
		{
			buffer[n] = '\0';
			bool isZNet =  std::string::npos != std::string(buffer).find("Znet");
			//printf("Server response: %s\n", buffer);
			std::string message(buffer);
			std::string product = takeString(&message);
			std::string mac_address = takeString(&message);
			std::string activation_code = takeString(&message);
			std::string firmware_version = takeString(&message);
			std::string serial = takeString(&message); // Or whaterver it is
			TellStickNetDescriptor td;
			td.serial = activation_code;
			td.mac_address = mac_address;
			td.ip = std::string(inet_ntoa(servaddr.sin_addr));
			td.haveRawCapabilities = ! isZNet;
			Log::debug("Found tellstick with ip = %s and mac %s", td.ip.c_str(),td.mac_address.c_str());
			retval.push_back(td);
		}
	}
	Log::debug("Returning to service");
	SOCKCLOSE(sockfd);
	return retval;
}

std::list<TellStickNetDescriptor> TellStickNet::findAll( ) {
	return findOneOrAll();
}


void TellStickNet::stop() {
	if (d->running) {
		{
			TelldusCore::MutexLocker locker(&d->mutex);
			d->running = false;
		}
		// Unlock the wait-condition
#ifdef _WINDOWS
		SetEvent(d->eh);
#else
		pthread_cond_broadcast(&d->eh.eCondVar);
#endif
	}
	this->wait();
}



