#ifndef DEVICEWIDGET_H
#define DEVICEWIDGET_H

#include <QWidget>
#include <QTableView>
#include <QToolButton>
#include <QCheckBox>
#include <QtCore/QSortFilterProxyModel>
#include "devicemodel.h"
#include "deviceview.h"


class DeviceWidget : public QWidget {
	Q_OBJECT
	Q_DISABLE_COPY(DeviceWidget)
public:
	explicit DeviceWidget(QWidget *parent = 0);
	virtual ~DeviceWidget();

signals:
	void showMessage( const QString &title, const QString &message, const QString &detailedMessage );
	void eventTriggered( const QString &name, const QString &title );

protected:
	virtual void changeEvent(QEvent *e);

private slots:
	void listActivated(const QModelIndex &);
	void addDevice();
	void addGroup();
	void deleteDevice();
	void editGroupOrDevice();
	void editDevice();
	void editGroup();
	void emitError();
	void showControllerStats(bool show);

private:
	DeviceModel model;
	QSortFilterProxyModel sortedModel;
	DeviceView deviceView;
	QToolButton addToolButton, removeToolButton, editToolButton;
	QCheckBox showStats;
	QAction *editDeviceMenuAction;
	QAction *editGroupMenuAction ;
	QAction *makeGroupMenuAction ;
};

#endif // DEVICEWIDGET_H
