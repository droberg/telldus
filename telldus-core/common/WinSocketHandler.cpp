#include "WinSocketHandler.h"
#ifdef _WINDOWS
#include "stdio.h"
#include <winsock2.h>
#include <Ws2tcpip.h>
#pragma comment(lib,"ws2_32.lib") //Winsock Library

WSADATA wsa;
#endif

bool isInitialized = false;

void initSocketsIfNecessary()
{
	if(! isInitialized)
	{
#if defined(_WINDOWS)
		if (WSAStartup(MAKEWORD(2,2),&wsa) != 0)
		{
			printf("Failed. Error Code : %d",WSAGetLastError());
		}
#endif
		isInitialized = true;
	}
}

void cleanupSocketsIfNecessary()
{
#if defined(_WINDOWS)
	if(isInitialized)
		WSACleanup();
#endif
	isInitialized = false;
}
