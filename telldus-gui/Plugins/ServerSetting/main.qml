import QtQuick 2.12
import QtQuick.Controls 2.12
import Telldus 1.0
Item {
	width: 500 //Minimum width

	Column {
		spacing: 1
		anchors.fill: parent

		BorderImage {
			id: header
			source: "header_bg.png"
			width: parent.width; height: 40
			border.left: 5; border.top: 5
			border.right: 5; border.bottom: 5
			HeaderTitle {
				text: "Serversettings"
				anchors.left: parent.left
				anchors.leftMargin: 15
			}
		}
		BorderImage {
			id: view
			source: "row_bg.png"
			border.left: 5; border.top: 5
			border.right: 5; border.bottom: 5
			width: parent.width
			height: content.height + content.anchors.margins*2
			Item {
				id: content
				anchors.top: parent.top
				anchors.left: parent.left
				anchors.margins: 5
				height: childrenRect.height
				width: childrenRect.width
				Column
				{
					Row {
						spacing: 10
						Image {
							source: "kde-oxygen-network-server.png"
							width: 50
							smooth: true
							fillMode: Image.PreserveAspectFit
							opacity: 1//controller.available ? 1 : 0.5
						}
						Column {
							Text {
								color: "#004275"
								text: "Server hostname/ip "//productName(controller.type)
								font.pixelSize: 15
							}
							MyObject {
								id: myobject
							}
							TextField {
								id: nameEdit
								text: myobject.getServerIP();
								placeholderText: 'Hostname for controller'
								onTextChanged: myobject.setServerIP(text)
							}
						}
					}
					Row {
						spacing: 10
						Text {
							color: "#b5121a"
							text: "Note: Restart needed after chage"
							font.pixelSize: 15
						}
					}
				}
			}
		}
	}
}
