//
// Copyright (C) 2012 Telldus Technologies AB. All rights reserved.
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef TELLDUS_CORE_SERVICE_SENSOR_H_
#define TELLDUS_CORE_SERVICE_SENSOR_H_

#ifdef __FreeBSD__
#include <ctime>
#endif
#include <string>
#include "common/Mutex.h"

class Sensor : public TelldusCore::Mutex {
public:
	Sensor(const std::wstring &protocol, const std::wstring &model, int id);
	~Sensor();

	std::wstring protocol() const;
	std::wstring model() const;
	int id() const;
	time_t timestamp(int type) const;
	std::wstring timestampStr() const;
	std::wstring name() const;

	int dataTypes() const;

	void setName(std::wstring name);
	bool setValue(int type, const std::string &value, time_t timestamp);
	std::string value(int type) const;
	void setStateValueMap(std::wstring stateValueString, std::wstring timeStamp);
	std::wstring stateValueMap() const;
	void setMaxValues(std::wstring maxValues);
	void setMinValues(std::wstring minValues);
	void setMaxRates(std::wstring maxRate);
	void setNOutlisersBeforeAccept(int nOutliers);

	static int stringToType(std::string type);

private:
	class PrivateData;
	PrivateData *d;
};

#endif  // TELLDUS_CORE_SERVICE_SENSOR_H_
