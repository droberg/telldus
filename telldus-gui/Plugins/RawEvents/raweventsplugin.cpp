#include "raweventsplugin.h"
#include <QScriptEngine>
#include <QQuickView>
#include <QSettings>

RawEventsPlugin::RawEventsPlugin ( QObject * parent )
    :QScriptExtensionPlugin( parent )
{
}

RawEventsPlugin::~RawEventsPlugin() {

}

void RawEventsPlugin::initialize ( const QString & key, QScriptEngine * engine ) {
	if (key == "com.telldus.rawevents") {
		qmlRegisterType<RawDataObject>("Telldus", 1, 0, "RawDataObject");

		QScriptValue qml = engine->globalObject().property("com").property("telldus").property("rawevents");
	}
}

QStringList RawEventsPlugin::keys () const {
	return QStringList() << "com.telldus.rawevents";
}
