import QtQuick 2.12
import QtQuick.Controls 2.12

Item {
	id: main
	state: initialViewMode

	ScrollView {
		id: scrollArea
		anchors.fill: parent

		//contentHeight: sensorList.height
		//contentWidth: sensorList.width

		SensorList {
			id: sensorList
			//property int calculatedWidth: scrollArea.availableWidth
			width: (scrollArea.availableWidth < minimumWidth ? minimumWidth : scrollArea.availableWidth)
		}
	}
}
