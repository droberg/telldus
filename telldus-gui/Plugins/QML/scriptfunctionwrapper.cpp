#include "scriptfunctionwrapper.h"

#include <QScriptContext>
#include <QScriptEngine>
#include <QDebug>

class ScriptFunctionWrapper::PrivateData {
public:
	QScriptValue object;
	QString name;
};

ScriptFunctionWrapper::ScriptFunctionWrapper(const QScriptValue &object, const QString &name, QObject *parent) :
	QObject(parent)
{
	d = new PrivateData;
	d->object = object;
	d->name = name;
}

ScriptFunctionWrapper::~ScriptFunctionWrapper() {
	delete d;
}

QScriptValue ScriptFunctionWrapper::call() {
	return d->object.property(d->name).call();
}

QScriptValue ScriptFunctionWrapper::callWith(const QScriptValue &val) {
	//qDebug() << "Call called" << d->name;
	QScriptEngine *eng = val.engine();
	if (!eng) {
		qDebug() << "We cannot access the script-engine, fail!";
		return QScriptValue();
	}
	QScriptContext *ctx = eng->currentContext();
	return d->object.property(d->name).call(QScriptValue(), ctx->argumentsObject());
}

QScriptValue ScriptFunctionWrapper::callWith_SSI(QString a, QString b, int c)
{
	qDebug() << "Call called" << d->name << " with a=" << a << ", b = " << b << ", c = " << c;
	QScriptValueList arguments;
	arguments.push_back(a);
	arguments.push_back(b);
	arguments.push_back(c);
	return d->object.property(d->name).call(QScriptValue(),arguments);
}

QScriptValue ScriptFunctionWrapper::callWith_I(int a)
{
	qDebug() << "Call called" << d->name << " with a=" << a ;
	QScriptValueList arguments;
	arguments.push_back(a);
	return d->object.property(d->name).call(QScriptValue(),arguments);
}

QString ScriptFunctionWrapper::callWith_S(QString a)
{
	qDebug() << "Call called" << d->name << " with a=" << a ;
	QScriptValueList arguments;
	arguments.push_back(a);
	QScriptValue retVal;
	retVal = d->object.property(d->name).call(QScriptValue(),arguments);
	qDebug() << "returning" << retVal.toString();
	return retVal.toString();
}
