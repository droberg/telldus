//
// Copyright (C) 2012 Telldus Technologies AB. All rights reserved.
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef TELLDUS_CORE_SERVICE_TELLSTICKNET_H_
#define TELLDUS_CORE_SERVICE_TELLSTICKNET_H_

#include <list>
#include <string>
#include "service/Controller.h"
#include "common/Thread.h"
#include "TellStick.h"

class TellStickNetDescriptor : public TellStickDescriptor{
public:
	virtual ~TellStickNetDescriptor();
	std::string ip; // This is not certain to be the same from time to time
	std::string mac_address;
	bool haveRawCapabilities;
};

class TellStickNet : public TellStick {
public:
	TellStickNet(int controllerId, TelldusCore::EventRef event, TelldusCore::EventRef updateEvent, const TellStickDescriptor *td);
	virtual ~TellStickNet();

	virtual bool haveRawCapability() const;
	virtual std::string ip() const;
	virtual std::string mac() const;
	virtual std::string serial() const;

	virtual bool isOpen() const;
	virtual bool isSameAsDescriptor(const TellStickDescriptor *td) const;
	virtual int reset();
	virtual int send( const std::string &message );
	virtual bool stillConnected() const;

	static std::list<TellStickNetDescriptor> findAll();
	static  std::list<TellStickNetDescriptor> findOneOrAll(std::string toIP = "");
	static std::string takeString(std::string *message) ;

protected:

	int setupConnection();
	void processData( const std::string &data );
	void run();
	void stop();
private:

	class PrivateData;
	PrivateData *d;
};

#endif  // TELLDUS_CORE_SERVICE_TELLSTICKNET_H_
