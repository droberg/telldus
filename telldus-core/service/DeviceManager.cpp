//
// Copyright (C) 2012 Telldus Technologies AB. All rights reserved.
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "service/DeviceManager.h"
#include <chrono>
#include <time.h>
#include <list>
#include <map>
#include <memory>
#include <queue>
#include <set>
#include <sstream>
#include <string>
#include "common/common.h"

#include "service/ControllerMessage.h"
#include "common/Mutex.h"
#include "service/Sensor.h"
#include "service/Settings.h"
#include "common/Strings.h"
#include "common/Message.h"
#include "service/Log.h"

typedef std::map<int, Device *> DeviceMap;
typedef std::map<int, Sensor *> SensorMap;
typedef std::pair<int, std::wstring> ActionRep;

class DeviceManager::PrivateData {
public:
	 DeviceMap devices;
	 SensorMap sensors;
	 Settings* set;
	 TelldusCore::Mutex lock, deviceEventAgeLock;
	 ControllerManager *controllerManager;
	 TelldusCore::EventRef deviceUpdateEvent, executeActionEvent;
	 std::map<int, int64_t > deviceEventAge;
	 bool ignoreNewSensors;
};

class ExecuteActionEventData : public TelldusCore::EventDataBase {
public:
	int deviceId;
	int method;
	unsigned char data;
};


DeviceManager::DeviceManager(ControllerManager *controllerManager, TelldusCore::EventRef deviceUpdateEvent) {
	d = new PrivateData;
	d->set = Settings::getInstance();
	d->controllerManager = controllerManager;
	d->deviceUpdateEvent = deviceUpdateEvent;
	d->ignoreNewSensors = d->set->getSetting(L"ignoreNewSensors") == L"true";
	fillDevices();
	fillSensors();
}

DeviceManager::~DeviceManager(void) {
	{
		TelldusCore::MutexLocker deviceListLocker(&d->lock);
		for (DeviceMap::iterator it = d->devices.begin(); it != d->devices.end(); ++it) {
			{TelldusCore::MutexLocker deviceLocker(it->second);}  // aquire lock, and release it, just to see that the device it's not in use anywhere
			delete(it->second);
		}
		for (SensorMap::iterator it = d->sensors.begin(); it != d->sensors.end(); ++it) {
			{TelldusCore::MutexLocker sensorLocker(it->second);}  // aquire lock, and release it, just to see that the device it's not in use anywhere
			delete(it->second);
		}
	}
	Settings::deleteInstance();
	delete d;
}

void DeviceManager::executeActionEvent() {
	Device *device = 0;
	TelldusCore::EventDataRef eventData = d->executeActionEvent->takeSignal();
	ExecuteActionEventData *data = dynamic_cast<ExecuteActionEventData*>(eventData.get());
	std::set<int> parsedDevices;
	if (!data) {
		Log::error("Could not cast executeAction data");
		return;
	}
	{
	std::auto_ptr<TelldusCore::MutexLocker> deviceLocker(0);
	{
		// devicelist locked
		TelldusCore::MutexLocker deviceListLocker(&d->lock);

		DeviceMap::iterator it = d->devices.find(data->deviceId);
		if (it == d->devices.end()) {
			return;
		}
		// device locked
		deviceLocker = std::auto_ptr<TelldusCore::MutexLocker>(new TelldusCore::MutexLocker(it->second));
		device = it->second;
	} // devicelist unlocked
	// If method is TELLSTICK_TURNON for dimmable and old dim-level available set dim to this level
	if (data->method == TELLSTICK_TURNON && device->isMethodSupported(TELLSTICK_DIM) > 0)
	{
		Log::debug("Switching TELLSTICK_TURNON to TELLSTICK_DIM");
		std::wstring oldStrValue = device->getStateValue();
		unsigned char oldValue = 255;
		// TELLSTICK_TURNON always sets data = 0 so this check is currently not necessary
		if (data->data == 0)
		{
			data->data = oldValue;
		}
		data->method = TELLSTICK_DIM;
		try {
			oldValue = std::stoi(oldStrValue);
			if(oldValue > 0) {
				Log::debug("Setting dim to %i, was %i", oldValue, data->data);
				data->data = oldValue;
			}
		} catch (std::invalid_argument) {
			Log::debug("Could not convert to int \"%ls\"", oldStrValue.c_str());
			//noop
		}
	}
	int preferredController = device->getPreferredControllerId();
	Controller *controller = d->controllerManager->getBestControllerById(preferredController, false);
	Log::notice("Execute a TellStick Action for device %i using controller %i ( 0 == any )", data->deviceId, preferredController);
	if(!controller)
	{
		Log::warning("No controllers found, cannot execute action for device %i", data->deviceId);
		return;
	}

	int retval = device->doAction(data->method, data->data, controller);
	if(retval == TELLSTICK_ERROR_BROKEN_PIPE) {
		Log::warning("Error in communication with TellStick when executing action. Resetting USB");
		d->controllerManager->resetController(controller);
	}
	if(retval == TELLSTICK_ERROR_BROKEN_PIPE || retval == TELLSTICK_ERROR_NOT_FOUND) {
		Log::warning("Rescanning USB ports");
		d->controllerManager->loadControllers();
		controller = d->controllerManager->getBestControllerById(device->getPreferredControllerId(), false);
		if(!controller) {
			Log::error("No contoller (TellStick) found, even after reset. Giving up.");
			return;
		}
		retval = device->doAction(data->method, data->data, controller);  // retry one more time
	}

	if(retval == TELLSTICK_SUCCESS && device->getMethods() & data->method) {
		// if method isn't explicitly supported by device, but used anyway as a fallback (i.e. bell), don't change state
		std::wstring datastring = TelldusCore::charUnsignedToWstring(data->data);
		if (updateDeviceStateIfNecessary(device, data->deviceId, data->method, datastring, -1)) 
			parsedDevices.insert(data->deviceId);
	}
	} // device unlocked
	triggerGroupStateUpdate(parsedDevices);
}

void DeviceManager::setExecuteActionEvent(TelldusCore::EventRef event) {
	d->executeActionEvent = event;
}

void DeviceManager::fillSensors() {
	int numberOfDevices = d->set->getNumberOfNodes(Settings::Sensor);
	TelldusCore::MutexLocker deviceListLocker(&d->lock);
	for (int i = 0; i < numberOfDevices; ++i) {
		int id = d->set->getNodeId(Settings::Sensor, i);
		d->sensors[id] = new Sensor(d->set->getProtocol(Settings::Sensor,id), d->set->getModel(Settings::Sensor,id), id);
		d->sensors[id]->setName(d->set->getName(Settings::Sensor, id));
		d->sensors[id]->setStateValueMap(d->set->getStateValueMap(id),d->set->getSensorTimestamp(id));
		d->sensors[id]->setMaxRates(d->set->getMaxRates(id));
		d->sensors[id]->setMaxValues(d->set->getMaxValues(id));
		d->sensors[id]->setMinValues(d->set->getMinValues(id));
		d->sensors[id]->setNOutlisersBeforeAccept(d->set->getNOutlisersBeforeAccept(id));
	}
}

void DeviceManager::fillDevices() {
	int numberOfDevices = d->set->getNumberOfNodes(Settings::Device);
	TelldusCore::MutexLocker deviceListLocker(&d->lock);

	for (int i = 0; i < numberOfDevices; ++i) {
		int id = d->set->getNodeId(Settings::Device, i);
		d->devices[id] = new Device(id);
		d->devices[id]->setName(d->set->getName(Settings::Device, id));
		d->devices[id]->setModel(d->set->getModel(Settings::Device,id));
		d->devices[id]->setProtocolName(d->set->getProtocol(Settings::Device,id));
		d->devices[id]->setPreferredControllerId(d->set->getPreferredControllerId(id));
		int unknownatStart =  d->set->getUnknownAtStart(id);
		printf("Unknownatstart = %i for device %i \n",unknownatStart,id);
		if(unknownatStart == 1)
		{
			d->devices[id]->setLastSentCommand(TELLSTICK_UNDEFINED, L"",-1);
		}
		else
		{
			d->devices[id]->setLastSentCommand(d->set->getDeviceState(id), d->set->getDeviceStateValue(id),-1);
		}
		d->devices[id]->setControllerStats(d->set->getControllerStats(id));
		d->devices[id]->setParameter(L"house", d->set->getDeviceParameter(id, L"house"));
		d->devices[id]->setParameter(L"unit", d->set->getDeviceParameter(id, L"unit"));
		d->devices[id]->setParameter(L"group", d->set->getDeviceParameter(id, L"group"));
		d->devices[id]->setParameter(L"code", d->set->getDeviceParameter(id, L"code"));
		d->devices[id]->setParameter(L"units", d->set->getDeviceParameter(id, L"units"));
		d->devices[id]->setParameter(L"fade", d->set->getDeviceParameter(id, L"fade"));
		d->devices[id]->setParameter(L"system", d->set->getDeviceParameter(id, L"system"));
		d->devices[id]->setParameter(L"devices", d->set->getDeviceParameter(id, L"devices"));
	}
}

int DeviceManager::getDeviceLastSentCommand(int deviceId, int methodsSupported) {
	TelldusCore::MutexLocker deviceListLocker(&d->lock);
	if (!d->devices.size()) {
			return TELLSTICK_ERROR_DEVICE_NOT_FOUND;
	}
	DeviceMap::iterator it = d->devices.find(deviceId);
	if (it != d->devices.end()) {
		TelldusCore::MutexLocker deviceLocker(it->second);
		return it->second->getLastSentCommand(methodsSupported);
	}
	return TELLSTICK_ERROR_DEVICE_NOT_FOUND;
}

std::wstring DeviceManager::getDeviceStateValue(int deviceId) {
	TelldusCore::MutexLocker deviceListLocker(&d->lock);
	if (!d->devices.size()) {
			return L"UNKNOWN";
	}
	DeviceMap::iterator it = d->devices.find(deviceId);
	if (it != d->devices.end()) {
		TelldusCore::MutexLocker deviceLocker(it->second);
		return it->second->getStateValue();
	}
	return L"UNKNOWN";
}

int DeviceManager::getDeviceMethods(int deviceId, int methodsSupported) {
	return Device::maskUnsupportedMethods(DeviceManager::getDeviceMethods(deviceId), methodsSupported);
}

int DeviceManager::getDeviceMethods(int deviceId) {
	std::set<int> duplicateDeviceIds;
	return DeviceManager::getDeviceMethods(deviceId, &duplicateDeviceIds);
}

int DeviceManager::getDeviceMethods(int deviceId, std::set<int> *duplicateDeviceIds) {
	int type = 0;
	int methods = 0;
	std::wstring deviceIds;
	std::wstring protocol;

	{
		// devices locked
		TelldusCore::MutexLocker deviceListLocker(&d->lock);

		if (!d->devices.size()) {
			return TELLSTICK_ERROR_DEVICE_NOT_FOUND;
		}
		DeviceMap::iterator it = d->devices.find(deviceId);
		if (it != d->devices.end()) {
			{
				TelldusCore::MutexLocker deviceLocker(it->second);
				type = it->second->getType();
				methods = it->second->getMethods();
				deviceIds = it->second->getParameter(L"devices");
				protocol = it->second->getProtocolName();
			}
		}
	}
	if(type == 0) {
		return 0;
	}
	if(type == TELLSTICK_TYPE_GROUP) {
		// get all methods that some device in the groups supports
		std::wstring deviceIdBuffer;
		std::wstringstream devicesstream(deviceIds);
		methods = 0;

		duplicateDeviceIds->insert(deviceId);

		while(std::getline(devicesstream, deviceIdBuffer, L',')) {
			int deviceIdInGroup = TelldusCore::wideToInteger(deviceIdBuffer);
			if(duplicateDeviceIds->count(deviceIdInGroup) == 1) {
				// action for device already executed, or will execute, do nothing to avoid infinite loop
				continue;
			}

			duplicateDeviceIds->insert(deviceIdInGroup);

			int deviceMethods = getDeviceMethods(deviceIdInGroup, duplicateDeviceIds);
			if(deviceMethods > 0) {
				methods |= deviceMethods;
			}
		}
		methods &= ~TELLSTICK_LEARN;
	}
	return methods;
}

std::wstring DeviceManager::getDeviceModel(int deviceId) {
	TelldusCore::MutexLocker deviceListLocker(&d->lock);
	if (!d->devices.size()) {
			return L"UNKNOWN";
	}
	DeviceMap::iterator it = d->devices.find(deviceId);
	if (it != d->devices.end()) {
		TelldusCore::MutexLocker deviceLocker(it->second);
		return it->second->getModel();
	}
	return L"UNKNOWN";
}

int DeviceManager::setDeviceModel(int deviceId, const std::wstring &model) {
	TelldusCore::MutexLocker deviceListLocker(&d->lock);
	if (!d->devices.size()) {
			return TELLSTICK_ERROR_DEVICE_NOT_FOUND;
	}
	DeviceMap::iterator it = d->devices.find(deviceId);
	if (it != d->devices.end()) {
		TelldusCore::MutexLocker deviceLocker(it->second);
		int ret = d->set->setModel(Settings::Device, deviceId, model);
		if (ret != TELLSTICK_SUCCESS) {
			return ret;
		}
		it->second->setModel(model);
	} else {
		return TELLSTICK_ERROR_DEVICE_NOT_FOUND;
	}

	return TELLSTICK_SUCCESS;
}

std::wstring DeviceManager::getDeviceName(int deviceId) {
	TelldusCore::MutexLocker deviceListLocker(&d->lock);
	if (!d->devices.size()) {
			return L"UNKNOWN";
	}
	DeviceMap::iterator it = d->devices.find(deviceId);
	if (it != d->devices.end()) {
		TelldusCore::MutexLocker deviceLocker(it->second);
		return it->second->getName();
	}
	return L"UNKNOWN";
}

int DeviceManager::setDeviceName(int deviceId, const std::wstring &name) {
	TelldusCore::MutexLocker deviceListLocker(&d->lock);
	if (!d->devices.size()) {
			return TELLSTICK_ERROR_DEVICE_NOT_FOUND;
	}
	DeviceMap::iterator it = d->devices.find(deviceId);
	if (it != d->devices.end()) {
		TelldusCore::MutexLocker deviceLocker(it->second);
		int ret = d->set->setName(Settings::Device, deviceId, name);
		if (ret != TELLSTICK_SUCCESS) {
			return ret;
		}
		it->second->setName(name);
	} else {
		return TELLSTICK_ERROR_DEVICE_NOT_FOUND;
	}

	return TELLSTICK_SUCCESS;
}

std::wstring DeviceManager::getDeviceParameter(int deviceId, const std::wstring &name, const std::wstring &defaultValue) {
	TelldusCore::MutexLocker deviceListLocker(&d->lock);
	if (!d->devices.size()) {
			return defaultValue;
	}
	DeviceMap::iterator it = d->devices.find(deviceId);
	if (it != d->devices.end()) {
		TelldusCore::MutexLocker deviceLocker(it->second);
		std::wstring returnString = it->second->getParameter(name);
		if(returnString != L"") {
			return returnString;
		}
	}
	return defaultValue;
}

std::wstring DeviceManager::getControllerStats(int deviceId) {
	TelldusCore::MutexLocker deviceListLocker(&d->lock);
	if (!d->devices.size()) {
			return L"Unknown";
	}
	DeviceMap::iterator it = d->devices.find(deviceId);
	if (it != d->devices.end()) {
		TelldusCore::MutexLocker deviceLocker(it->second);
		std::wstring returnString = TelldusCore::charToWstring( it->second->getControllerStats().c_str());
		if(returnString != L"") {
			return returnString;
		}
	}
	return L"Unknown";
}

int DeviceManager::clearControllerStats(int deviceId) {
	TelldusCore::MutexLocker deviceListLocker(&d->lock);
	if (!d->devices.size()) {
			return TELLSTICK_ERROR_DEVICE_NOT_FOUND;
	}
	DeviceMap::iterator it = d->devices.find(deviceId);
	if (it != d->devices.end()) {
		TelldusCore::MutexLocker deviceLocker(it->second);
		it->second->setControllerStats("");
		d->set->setDeviceState(deviceId, it->second->getState(), it->second->getStateValue(),"");
		return TELLSTICK_SUCCESS;
	}
	return TELLSTICK_ERROR_DEVICE_NOT_FOUND;
}

int DeviceManager::setDeviceParameter(int deviceId, const std::wstring &name, const std::wstring &value) {
	TelldusCore::MutexLocker deviceListLocker(&d->lock);
	if (!d->devices.size()) {
			return TELLSTICK_ERROR_DEVICE_NOT_FOUND;
	}
	DeviceMap::iterator it = d->devices.find(deviceId);
	if (it != d->devices.end()) {
		TelldusCore::MutexLocker deviceLocker(it->second);
		int ret = d->set->setDeviceParameter(deviceId, name, value);
		if (ret != TELLSTICK_SUCCESS) {
			return ret;
		}
		it->second->setParameter(name, value);
	} else {
		return TELLSTICK_ERROR_DEVICE_NOT_FOUND;
	}

	return TELLSTICK_SUCCESS;
}

std::wstring DeviceManager::getDeviceProtocol(int deviceId) {
	TelldusCore::MutexLocker deviceListLocker(&d->lock);
	if (!d->devices.size()) {
			return L"UNKNOWN";
	}
	DeviceMap::iterator it = d->devices.find(deviceId);
	if (it != d->devices.end()) {
		TelldusCore::MutexLocker deviceLocker(it->second);
		return it->second->getProtocolName();
	}
	return L"UNKNOWN";
}

int DeviceManager::setDeviceProtocol(int deviceId, const std::wstring &protocol) {
	TelldusCore::MutexLocker deviceListLocker(&d->lock);
	if (!d->devices.size()) {
			return TELLSTICK_ERROR_DEVICE_NOT_FOUND;
	}
	DeviceMap::iterator it = d->devices.find(deviceId);
	if (it != d->devices.end()) {
		TelldusCore::MutexLocker deviceLocker(it->second);
		int ret = d->set->setProtocol(Settings::Device, deviceId, protocol);
		if (ret != TELLSTICK_SUCCESS) {
			return ret;
		}
		it->second->setProtocolName(protocol);
	} else {
		return TELLSTICK_ERROR_DEVICE_NOT_FOUND;
	}

	return TELLSTICK_SUCCESS;
}

int DeviceManager::getNumberOfDevices() {
	TelldusCore::MutexLocker deviceListLocker(&d->lock);
	return static_cast<int>(d->devices.size());
}

int DeviceManager::addDevice() {
	int id = d->set->addNode(Settings::Device);
	if(id < 0) {
		return id;
	}

	TelldusCore::MutexLocker deviceListLocker(&d->lock);
	d->devices[id] = new Device(id);
	if(!d->devices[id]) {
		return TELLSTICK_ERROR_DEVICE_NOT_FOUND;
	}
	return id;
}

int DeviceManager::getDeviceId(int deviceIndex) {
	return d->set->getNodeId(Settings::Device, deviceIndex);
}

int DeviceManager::getDeviceType(int deviceId) {
	TelldusCore::MutexLocker deviceListLocker(&d->lock);
	if (!d->devices.size()) {
		return TELLSTICK_ERROR_DEVICE_NOT_FOUND;
	}
	DeviceMap::iterator it = d->devices.find(deviceId);
	if (it != d->devices.end()) {
		TelldusCore::MutexLocker deviceLocker(it->second);
		return it->second->getType();
	}
	return TELLSTICK_ERROR_DEVICE_NOT_FOUND;
}

int DeviceManager::getPreferredControllerId(int deviceId) {
	TelldusCore::MutexLocker deviceListLocker(&d->lock);

	if (!d->devices.size()) {
			return TELLSTICK_ERROR_DEVICE_NOT_FOUND;
	}
	DeviceMap::iterator it = d->devices.find(deviceId);
	if (it != d->devices.end()) {
		TelldusCore::MutexLocker deviceLocker(it->second);
		return it->second->getPreferredControllerId();
	}
	return TELLSTICK_ERROR_DEVICE_NOT_FOUND;
}

int DeviceManager::setPreferredControllerId(int deviceId, int preferredControllerId) {
	TelldusCore::MutexLocker deviceListLocker(&d->lock);
	if (!d->devices.size()) {
			return TELLSTICK_ERROR_DEVICE_NOT_FOUND;
	}
	DeviceMap::iterator it = d->devices.find(deviceId);
	if (it != d->devices.end()) {
		TelldusCore::MutexLocker deviceLocker(it->second);
		int ret = d->set->setPreferredControllerId(deviceId, preferredControllerId);
		if (ret != TELLSTICK_SUCCESS) {
			return ret;
		}
		it->second->setPreferredControllerId(preferredControllerId);
	} else {
		return TELLSTICK_ERROR_DEVICE_NOT_FOUND;
	}
	return TELLSTICK_SUCCESS;
}



void DeviceManager::connectTellStickController(int vid, int pid, const std::string &serial) {
	d->controllerManager->deviceInsertedOrRemoved(vid, pid, serial, true);
}

void DeviceManager::disconnectTellStickController(int vid, int pid, const std::string &serial) {
	d->controllerManager->deviceInsertedOrRemoved(vid, pid, serial, false);
}

int DeviceManager::doAction(int deviceId, int action, unsigned char data) {
	int deviceType = 0;
	int retVal = TELLSTICK_SUCCESS;
	{
		// devicelist locked
		TelldusCore::MutexLocker deviceListLocker(&d->lock);

		DeviceMap::iterator it = d->devices.find(deviceId);
		if (it == d->devices.end()) {
			return TELLSTICK_ERROR_DEVICE_NOT_FOUND;  // not found
		}
		// device locked
		TelldusCore::MutexLocker deviceLocker(it->second);

		deviceType = it->second->getType();
		if (it->second->isMethodSupported(action) <= 0) {
			return TELLSTICK_ERROR_METHOD_NOT_SUPPORTED;
		}
	}

	if(deviceType == TELLSTICK_TYPE_DEVICE || deviceType == TELLSTICK_TYPE_DEVICEGROUP) {
		if (d->controllerManager->count() != 0) {
			ExecuteActionEventData *eventData = new ExecuteActionEventData();
			eventData->deviceId = deviceId;
			eventData->method = action;
			eventData->data = data;
			d->executeActionEvent->signal(eventData);
		}
		else {
			retVal = TELLSTICK_ERROR_NOT_FOUND;
		}
	}

	// The device exists and there is at least one connected controller

	if(deviceType == TELLSTICK_TYPE_GROUP || deviceType == TELLSTICK_TYPE_SCENE || deviceType == TELLSTICK_TYPE_DEVICEGROUP) {
		retVal = this->doGroupSceneAction(deviceId, action, data);
	}
	return retVal;
}

int DeviceManager::doGroupSceneAction(int deviceId, int action, unsigned char data) {
	// This is for a top-level group, if there are groups below this group, 
	// all devices of those groups are assumed to respond to the code of this group
	std::set<int> parsedDevices;
	std::queue<int> devicesToParse;
	bool isDeviceGroup = false;
	{
		TelldusCore::MutexLocker deviceListLocker(&d->lock);
		DeviceMap::iterator it = d->devices.find(deviceId);
		if (it == d->devices.end()) {
			// Not found
			return TELLSTICK_ERROR_NOT_FOUND;
		}
		else
		{
			isDeviceGroup = it->second->getType() == TELLSTICK_TYPE_DEVICEGROUP;
		}
	}

	devicesToParse.push(deviceId);
	while (!devicesToParse.empty()) {
		int deviceId = devicesToParse.front();
		devicesToParse.pop();
		if (parsedDevices.count(deviceId)) {
			// Don't go looping in circles
			continue;
		}
		parsedDevices.insert(deviceId);

		TelldusCore::MutexLocker deviceListLocker(&d->lock);
		DeviceMap::iterator it = d->devices.find(deviceId);
		if (it == d->devices.end()) {
			// Not found
			continue;
		}

		// Try to find a supported action
		int closestSupportedAction = action;
		int closestSupportedActionData = data;
		std::wstring closestSupportedActionDatastring = TelldusCore::charUnsignedToWstring(closestSupportedActionData);
		if(action == TELLSTICK_DIM && it->second->isMethodSupported(TELLSTICK_DIM) <= 0)
		{
			closestSupportedAction = data > 0 ? TELLSTICK_TURNON : TELLSTICK_TURNOFF ;
			closestSupportedActionData = 0;
		}
		// If still not supported, then skip
		if (it->second->isMethodSupported(closestSupportedAction) <= 0) {
			continue;
		}

		TelldusCore::MutexLocker deviceLocker(it->second);
		if (it->second->getType() == TELLSTICK_TYPE_DEVICE) {
			if( ! isDeviceGroup ) {
				// Schedule an action request to controllers, this will update states later
				ExecuteActionEventData *eventData = new ExecuteActionEventData();
				eventData->deviceId = deviceId;
				eventData->method = closestSupportedAction;
				eventData->data = closestSupportedActionData;
				d->executeActionEvent->signal(eventData);
			} else if(it->second->getMethods() & closestSupportedAction) {
				// Update state directly, the signal is already sent
				// if method isn't explicitly supported by device, but used anyway as a fallback (i.e. bell), don't change state
				updateDeviceStateIfNecessary(it->second, deviceId, closestSupportedAction, closestSupportedActionDatastring,-1);
			}
			continue;
		}
		if (it->second->getType() == TELLSTICK_TYPE_GROUP || it->second->getType() == TELLSTICK_TYPE_DEVICEGROUP) {
			std::string devices = TelldusCore::wideToString(it->second->getParameter(L"devices"));
			std::stringstream devicesstream(devices);
			std::string singledevice;
			while(std::getline(devicesstream, singledevice, ',')) {
				devicesToParse.push(TelldusCore::charToInteger(singledevice.c_str()));
			}
			// Update state
			if(it->second->getMethods() & closestSupportedAction) {
				// if method isn't explicitly supported by device, but used anyway as a fallback (i.e. bell), don't change state
				updateDeviceStateIfNecessary(it->second, deviceId, closestSupportedAction, closestSupportedActionDatastring,-1);
			}
		}
		if (it->second->getType() == TELLSTICK_TYPE_SCENE) {
			// TODO(micke): Not supported yet
			Log::warning("Scenes are not supported yet!");
		}
	}
	triggerGroupStateUpdate(parsedDevices);
	return TELLSTICK_SUCCESS;
}

int DeviceManager::executeScene(std::wstring singledevice, int groupDeviceId) {
	std::wstringstream devicestream(singledevice);

	const int deviceParameterLength = 3;
	std::wstring deviceParts[deviceParameterLength] = {L"", L"", L""};
	std::wstring devicePart = L"";
	int i = 0;
	while(std::getline(devicestream, devicePart, L':') && i < deviceParameterLength) {
		deviceParts[i] = devicePart;
		i++;
	}

	if(deviceParts[0] == L"" || deviceParts[1] == L"") {
		return TELLSTICK_ERROR_UNKNOWN;  // malformed or missing parameter
	}

	int deviceId = TelldusCore::wideToInteger(deviceParts[0]);
	if(deviceId == groupDeviceId) {
		return TELLSTICK_ERROR_UNKNOWN;  // the scene itself has been added to its devices, avoid infinite loop
	}
	int method = Device::methodId(TelldusCore::wideToString(deviceParts[1]));  // support methodparts both in the form of integers (e.g. TELLSTICK_TURNON) or text (e.g. "turnon")
	if(method == 0) {
		method = TelldusCore::wideToInteger(deviceParts[1]);
	}
	unsigned char devicedata = 0;
	if(deviceParts[2] != L"") {
		devicedata = TelldusCore::wideToInteger(deviceParts[2]);
	}

	if(deviceId > 0 && method > 0) {  // check for format error in parameter "devices"
		return doAction(deviceId, method, devicedata);
	}

	return TELLSTICK_ERROR_UNKNOWN;
}

int DeviceManager::removeDevice(int deviceId) {
	Device *device = 0;
	{
		int ret = d->set->removeNode(Settings::Device, deviceId);  // remove from register/settings
		if (ret != TELLSTICK_SUCCESS) {
			return ret;
		}

		TelldusCore::MutexLocker deviceListLocker(&d->lock);
		if (!d->devices.size()) {
				return TELLSTICK_ERROR_DEVICE_NOT_FOUND;
		}
		DeviceMap::iterator it = d->devices.find(deviceId);
		if (it != d->devices.end()) {
			device = it->second;
			d->devices.erase(it);  // remove from list, keep reference
		} else {
			return TELLSTICK_ERROR_DEVICE_NOT_FOUND;
		}
	}
	{TelldusCore::MutexLocker lock(device);}  // waiting for device lock, if it's aquired, just unlock again. Device is removed from list, and cannot be accessed from anywhere else
	delete device;

	return TELLSTICK_SUCCESS;
}

std::wstring DeviceManager::getSensors() const {
	TelldusCore::MutexLocker sensorListLocker(&d->lock);

	TelldusCore::Message msg;

	msg.addArgument(static_cast<int>(d->sensors.size()));

	for (SensorMap::iterator it = d->sensors.begin(); it != d->sensors.end(); ++it) {
		TelldusCore::MutexLocker sensorLocker(it->second);
		msg.addArgument(it->second->protocol());
		msg.addArgument(it->second->model());
		msg.addArgument(it->second->name());
		msg.addArgument(it->second->id());
		msg.addArgument(it->second->dataTypes());
	}

	return msg;
}

std::wstring DeviceManager::getSensorValue(const std::wstring &protocol, const std::wstring &model, int id, int dataType) const {
	TelldusCore::MutexLocker sensorListLocker(&d->lock);
	Sensor *sensor = 0;
	for (SensorMap::iterator it = d->sensors.begin(); it != d->sensors.end(); ++it) {
		TelldusCore::MutexLocker sensorLocker(it->second);
		if (!TelldusCore::comparei((it->second)->protocol(), protocol)) {
			continue;
		}
		if (!TelldusCore::comparei(it->second->model(), model)) {
			continue;
		}
		if (it->second->id() != id) {
			continue;
		}
		sensor = it->second;
		break;
	}

	if (!sensor) {
		return L"";
	}
	TelldusCore::MutexLocker sensorLocker(sensor);
	TelldusCore::Message msg;
	std::string value = sensor->value(dataType);
	if (value.length() > 0) {
		msg.addArgument(TelldusCore::charToWstring(value.c_str()));
		msg.addArgument(static_cast<int>(sensor->timestamp(dataType)));
	}
	return msg;
}

std::wstring DeviceManager::getSensorName(int deviceId)
{
	TelldusCore::MutexLocker deviceListLocker(&d->lock);
	if (!d->sensors.size()) {
			return L"UNKNOWN";
	}
	SensorMap::iterator it = d->sensors.find(deviceId);
	if (it != d->sensors.end()) {
		TelldusCore::MutexLocker deviceLocker(it->second);
		return it->second->name();
	}
	return L"UNKNOWN";
}

int DeviceManager::setSensorName(int deviceId, const std::wstring &name)
{
	TelldusCore::MutexLocker deviceListLocker(&d->lock);
	if (!d->sensors.size()) {
			return TELLSTICK_ERROR_DEVICE_NOT_FOUND;
	}
	SensorMap::iterator it = d->sensors.find(deviceId);
	if (it != d->sensors.end()) {
		TelldusCore::MutexLocker deviceLocker(it->second);
		int ret = d->set->setName(Settings::Sensor, deviceId, name);
		if (ret != TELLSTICK_SUCCESS) {
			return ret;
		}
		it->second->setName(name);
	} else {
		return TELLSTICK_ERROR_DEVICE_NOT_FOUND;
	}

	return TELLSTICK_SUCCESS;
}

std::wstring DeviceManager::getSensorProtocol(int deviceId)
{
	TelldusCore::MutexLocker deviceListLocker(&d->lock);
	if (!d->sensors.size()) {
			return L"UNKNOWN";
	}
	SensorMap::iterator it = d->sensors.find(deviceId);
	if (it != d->sensors.end()) {
		TelldusCore::MutexLocker deviceLocker(it->second);
		return it->second->protocol();
	}
	return L"UNKNOWN";
}

std::wstring DeviceManager::getSensorModel(int deviceId)
{
	TelldusCore::MutexLocker deviceListLocker(&d->lock);
	if (!d->sensors.size()) {
			return L"UNKNOWN";
	}
	SensorMap::iterator it = d->sensors.find(deviceId);
	if (it != d->sensors.end()) {
		TelldusCore::MutexLocker deviceLocker(it->second);
		return it->second->model();
	}
	return L"UNKNOWN";
}

void DeviceManager::handleControllerMessage(const ControllerEventData &eventData) {
	// Trigger raw-event
	std::set<int> parsedDevices;
	EventUpdateData *eventUpdateData = new EventUpdateData();
	eventUpdateData->messageType = L"TDRawDeviceEvent";
	eventUpdateData->controllerId = eventData.controllerId;
	eventUpdateData->eventValue = TelldusCore::charToWstring(eventData.msg.c_str());
	d->deviceUpdateEvent->signal(eventUpdateData);

	ControllerMessage msg(eventData.msg);
	if (msg.msgClass().compare("sensor") == 0) {
		handleSensorMessage(msg);
		return;
	}

	{
	TelldusCore::MutexLocker deviceListLocker(&d->lock);
	for (DeviceMap::iterator it = d->devices.begin(); it != d->devices.end(); ++it) {
		TelldusCore::MutexLocker deviceLocker(it->second);
		if (!TelldusCore::comparei(it->second->getProtocolName(), msg.protocol())) {
			continue;
		}
		if ( !(it->second->getMethods() & msg.method()) ) {
			continue;
		}

		std::list<std::string> parameters = it->second->getParametersForProtocol();
		bool thisDevice = true;
		for (std::list<std::string>::iterator paramIt = parameters.begin(); paramIt != parameters.end(); ++paramIt) {
			if(!TelldusCore::comparei(it->second->getParameter(TelldusCore::charToWstring((*paramIt).c_str())), TelldusCore::charToWstring(msg.getParameter(*paramIt).c_str()))) {
				thisDevice = false;
				break;
			}
		}

		if(!thisDevice) {
			continue;
		}

		updateDeviceStateIfNecessary(it->second, it->first, msg.method(), L"",eventData.controllerId);
		parsedDevices.insert(it->first); // Announce the group state even if the state turned out to be the same after switch
		if(it->second->getType() == TELLSTICK_TYPE_DEVICEGROUP )
		{
			std::string devices = TelldusCore::wideToString(it->second->getParameter(L"devices"));
			std::stringstream devicesstream(devices);
			std::string singledevice;
			while(std::getline(devicesstream, singledevice, ',')) {
				DeviceMap::iterator groupDeviceIt = d->devices.find(TelldusCore::charToInteger(singledevice.c_str()));
				if (groupDeviceIt != d->devices.end()) {
					updateDeviceStateIfNecessary(groupDeviceIt->second, groupDeviceIt->first, msg.method(), L"",eventData.controllerId);
					parsedDevices.insert(groupDeviceIt->first); // Announce the group state even if the state turned out to be the same after switch
				}
			}
		}
	}
	}
	if(parsedDevices.size() > 0)
		triggerGroupStateUpdate(parsedDevices);
}

void DeviceManager::handleSensorMessage(const ControllerMessage &msg) {
	TelldusCore::MutexLocker sensorListLocker(&d->lock);
	Sensor *sensor = 0;
	for (SensorMap::iterator it = d->sensors.begin(); it != d->sensors.end(); ++it) {
		TelldusCore::MutexLocker sensorLocker(it->second);
		if (!TelldusCore::comparei((it->second)->protocol(), msg.protocol())) {
			continue;
		}
		if (!TelldusCore::comparei((it->second)->model(), msg.model())) {
			continue;
		}
		if ((it->second)->id() != strtol(msg.getParameter("id").c_str(), NULL, 10)) {
			continue;
		}
		sensor = it->second;
		break;
	}

	if (!sensor) {
		if( d->ignoreNewSensors )
			return;
		int sensorId = strtol(msg.getParameter("id").c_str(), NULL, 10);
		sensor = new Sensor(msg.protocol(), msg.model(), sensorId);
		d->sensors[sensorId] = sensor;
		d->set->addNode(Settings::Sensor,sensorId);
		d->set->setProtocol(Settings::Sensor,sensorId, sensor->protocol());
		d->set->setModel(Settings::Sensor,sensorId, sensor->model());
		// Read back default settings
		d->sensors[sensorId]->setMaxRates(d->set->getMaxRates(sensorId));
		d->sensors[sensorId]->setMaxValues(d->set->getMaxValues(sensorId));
		d->sensors[sensorId]->setMinValues(d->set->getMinValues(sensorId));
		d->sensors[sensorId]->setNOutlisersBeforeAccept(d->set->getNOutlisersBeforeAccept(sensorId));
		sendSensorChangeSignal(sensorId, TELLSTICK_SENSOR_ADDED, 0);
	}
	TelldusCore::MutexLocker sensorLocker(sensor);

	time_t t = time(NULL);

	setSensorValueAndSignal("temp", TELLSTICK_TEMPERATURE, sensor, msg, t);
	setSensorValueAndSignal("humidity", TELLSTICK_HUMIDITY, sensor, msg, t);
	setSensorValueAndSignal("rainrate", TELLSTICK_RAINRATE, sensor, msg, t);
	setSensorValueAndSignal("raintotal", TELLSTICK_RAINTOTAL, sensor, msg, t);
	setSensorValueAndSignal("winddirection", TELLSTICK_WINDDIRECTION, sensor, msg, t);
	setSensorValueAndSignal("windaverage", TELLSTICK_WINDAVERAGE, sensor, msg, t);
	setSensorValueAndSignal("windgust", TELLSTICK_WINDGUST, sensor, msg, t);
	setSensorValueAndSignal("battery", TELLSTICK_BATTERY, sensor, msg, t);
	if(! d->set->setSensorState(sensor->id(), sensor->timestampStr(),sensor->stateValueMap()) ){
		// Write it again to update file correctly (this creates the sensor entry if its missing )
		d->set->setSensorState(sensor->id(), sensor->timestampStr(),sensor->stateValueMap() );
	};
}

void DeviceManager::setSensorValueAndSignal( const std::string &dataType, int dataTypeId, Sensor *sensor, const ControllerMessage &msg, time_t timestamp) const {
	if (!msg.hasParameter(dataType)) {
		return;
	}
	if(! sensor->setValue(dataTypeId, msg.getParameter(dataType), timestamp))
		return;

	EventUpdateData *eventData = new EventUpdateData();
	eventData->messageType = L"TDSensorEvent";
	eventData->protocol = sensor->protocol();
	eventData->model = sensor->model();
	eventData->name = sensor->name();
	eventData->sensorId = sensor->id();
	eventData->dataType = dataTypeId;
	eventData->value = TelldusCore::charToWstring(sensor->value(dataTypeId).c_str());
	eventData->timestamp = static_cast<int>(timestamp);
	d->deviceUpdateEvent->signal(eventData);
}

void DeviceManager::sendSensorChangeSignal(int deviceId, int eventDeviceChanges, int eventChangeType) {
	EventUpdateData *eventData = new EventUpdateData();
	eventData->messageType = L"TDSensorChangeEvent";
	eventData->deviceId = deviceId;
	eventData->eventDeviceChanges = eventDeviceChanges;
	eventData->eventChangeType = eventChangeType;
	d->deviceUpdateEvent->signal(eventData);
}

int DeviceManager::sendRawCommand(const std::wstring &command, int reserved) {
	Controller *controller = d->controllerManager->getBestControllerById(-1, true);

	if(!controller) {
		// no controller found, scan for one, and retry once
		d->controllerManager->loadControllers();
		controller = d->controllerManager->getBestControllerById(-1, true);
	}

	int retval = TELLSTICK_ERROR_UNKNOWN;
	if(controller) {
		retval = controller->send(TelldusCore::wideToString(command));
		if(retval == TELLSTICK_ERROR_BROKEN_PIPE) {
			d->controllerManager->resetController(controller);
		}
		if(retval == TELLSTICK_ERROR_BROKEN_PIPE || retval == TELLSTICK_ERROR_NOT_FOUND) {
			d->controllerManager->loadControllers();
			controller = d->controllerManager->getBestControllerById(-1, true);
			if(!controller) {
				return TELLSTICK_ERROR_NOT_FOUND;
			}
			retval = controller->send(TelldusCore::wideToString(command));  // retry one more time
		}
		return retval;
	} else {
		return TELLSTICK_ERROR_NOT_FOUND;
	}
}

int DeviceManager::setIgnoreNewSensors(bool ignore)
{
	d->ignoreNewSensors = ignore;
	return d->set->setGlobalSetting(L"ignoreNewSensors", ignore ? L"true" : L"false" );
}

std::wstring DeviceManager::getGlobalSettings(std::wstring name)
{
	return d->set->getSetting(name );
}


bool DeviceManager::triggerDeviceStateChange(int deviceId, int intDeviceState, const std::wstring &strDeviceStateValue ) {
	if ( intDeviceState == TELLSTICK_BELL || intDeviceState == TELLSTICK_LEARN || intDeviceState == TELLSTICK_EXECUTE) {
		return false;
	}

	EventUpdateData *eventData = new EventUpdateData();
	eventData->messageType = L"TDDeviceEvent";
	eventData->eventState = intDeviceState;
	eventData->deviceId = deviceId;
	eventData->eventValue = strDeviceStateValue;
	d->deviceUpdateEvent->signal(eventData);

	EventUpdateData *deviceStatsChanged = new EventUpdateData();
	deviceStatsChanged->messageType = L"TDDeviceChangeEvent";
	deviceStatsChanged->eventState = intDeviceState;
	deviceStatsChanged->deviceId = deviceId;
	deviceStatsChanged->eventDeviceChanges = TELLSTICK_DEVICE_CHANGED;
	deviceStatsChanged->eventChangeType = TELLSTICK_CHANGE_CONTROLLERSTATS;
	d->deviceUpdateEvent->signal(deviceStatsChanged);
	return true;
}

bool DeviceManager::updateDeviceStateIfNecessary(Device *device, int id, int method, std::wstring value, int controllerId)
{
	long long int timeNow = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::steady_clock::now().time_since_epoch()).count();
	bool announceDeviceChange = false;
	bool deviceNeededUpdate = false;
	{
					TelldusCore::MutexLocker deviceListLocker(&d->deviceEventAgeLock);
					std::map<int, int64_t>::iterator it = d->deviceEventAge.find(id);
					if (it == d->deviceEventAge.end() || (( timeNow - it->second ) > 250 ) ) // if older than 250ms
					{
									announceDeviceChange = true;
					}
					d->deviceEventAge[id] = timeNow;
	}

	if(method == TELLSTICK_TURNOFF && device->isMethodSupported(TELLSTICK_DIM) > 0)
		value = device->getStateValue(); // Save old dim-level

	if( (deviceNeededUpdate = device->setLastSentCommand(method, value, controllerId)) )
	{
		announceDeviceChange = true;
		Log::debug("Device %i needed update of state",id);
	}

	if( deviceNeededUpdate || ( controllerId != -1 )) 
	{
		// Update device state and statistics
		d->set->setDeviceState(id, method, value, device->getControllerStats());
	}

	if( announceDeviceChange ) {
		Log::debug("Announcing update of %i",id);
		triggerDeviceStateChange(id,method,value);
	}
	else
		Log::debug("Not announcing %i",id);
	return deviceNeededUpdate;
}

void DeviceManager::triggerGroupStateUpdate(std::set<int> parsedDevices)
{
	//Log::debug("triggerGroupStateUpdate:");
	//for (std::set<int>::iterator it=parsedDevices.begin(); it!=parsedDevices.end(); ++it)
	//{
	//	Log::debug("Device: %i", *it);
	//}
	TelldusCore::MutexLocker deviceListLocker(&d->lock);
	for (DeviceMap::iterator it = d->devices.begin(); it != d->devices.end(); ++it) {
		TelldusCore::MutexLocker deviceLocker(it->second);
		if (it->second->getType() != TELLSTICK_TYPE_DEVICEGROUP && it->second->getType() != TELLSTICK_TYPE_GROUP)  {
			continue;
		}
		bool anyParsedDeviceIsInGroup = false;
		//Log::debug("Checking to see if we need update of group: %i", *it);
		std::set<std::pair<int,std::wstring>> actions;
		std::string devices = TelldusCore::wideToString(it->second->getParameter(L"devices"));
		std::stringstream devicesstream(devices);
		std::string singledevice;
		while(std::getline(devicesstream, singledevice, ',')) {
			DeviceMap::iterator it = d->devices.find( TelldusCore::charToInteger( singledevice.c_str() ));
			anyParsedDeviceIsInGroup |= parsedDevices.find(it->first) !=  parsedDevices.end();
			//Log::debug("Handling device: %i", *it);
			if (it != d->devices.end() ) {
				int state = it->second->getState();
				std::wstring stateValue =  it->second->getStateValue();
				// Skip state value of group members with turnon/turnonff for group compare
				if ( state == TELLSTICK_TURNOFF || state == TELLSTICK_TURNON ) {
					stateValue = L"";
				}
				// If light is dimmed with other value than 0 or undefined it's
				// considered as beeing on for group comparison, otherwise it's considered as beeing off
				if ( state == TELLSTICK_DIM ) {
					unsigned char stateValInt = 0;
					try {
						stateValInt = std::stoi(stateValue);
						stateValue = L"";
						if(stateValInt == 0){
							state = TELLSTICK_TURNOFF;
						}
						else {
							state = TELLSTICK_TURNON;
						}
					} catch (std::invalid_argument) {
						state = TELLSTICK_TURNOFF;
						stateValue = L"";
					}
				}
				actions.insert(ActionRep(state, stateValue));
			}
		}
		ActionRep stateRep;
		if(actions.size() == 1)
			stateRep = *(actions.begin());
		else {
			stateRep = ActionRep(TELLSTICK_UNDEFINED,L"");
		}
		//if(anyParsedDeviceIsInGroup)
		//{
		//	Log::debug("Device is in group %i", it->first);
		//}
		if(anyParsedDeviceIsInGroup && ! updateDeviceStateIfNecessary(it->second , it->first, stateRep.first, stateRep.second, -1))
			Log::debug("Group no change %i", it->first);
	}
}
