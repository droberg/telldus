//
// Copyright (C) 2012 Telldus Technologies AB. All rights reserved.
//
// Copyright: See COPYING file that comes with this distribution
//
//


#include <sys/types.h>

#include <errno.h>
#include <fcntl.h>
#include <math.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#ifndef  _WINDOWS
#include <unistd.h>
#endif

#include "common/WinSocketHandler.h"
#include "common/Socket.h"

#include "service/ConnectionListener.h"
#include "service/Log.h"
#include "client/telldus-core.h"

#if (defined(_MACOSX) || defined (__FreeBSD__)) && !defined(SOCK_CLOEXEC)
#define SOCK_CLOEXEC 0
#endif

class ConnectionListener::PrivateData {
public:
	TelldusCore::EventRef waitEvent;
	std::string name;
	bool running;
};

ConnectionListener::ConnectionListener(const std::wstring &name, TelldusCore::EventRef waitEvent) {
	d = new PrivateData;
	d->waitEvent = waitEvent;

	d->name = "/tmp/" + std::string(name.begin(), name.end());
	d->running = true;

	this->start();
}

ConnectionListener::~ConnectionListener(void) {
	d->running = false;
	this->wait();
	unlink(d->name.c_str());
	delete d;
}

void ConnectionListener::run() {
	struct timeval tv = { 0, 0 };
	initSocketsIfNecessary();
	// Timeout for select

	SOCKET_T serverSocket;
	struct sockaddr_in name;
	serverSocket = socket(PF_INET, SOCK_STREAM , IPPROTO_TCP);
	if (serverSocket < 0) {
		return;
	}
	setsockoptType optval = 1;
	setsockopt(serverSocket, SOL_SOCKET, SO_REUSEADDR, &optval, sizeof(optval));

	memset(&name, 0, sizeof(name));
	name.sin_addr.s_addr = htonl(INADDR_ANY);
	name.sin_family = PF_INET;
	if(d->name.compare("/tmp/TelldusClient") == 0) {
		name.sin_port = htons(TELLSTICK_DEFAULT_CLIENT_PORT);
	} else if (d->name.compare("/tmp/TelldusEvents") == 0){
		name.sin_port = htons(TELLSTICK_DEFAULT_EVENT_PORT);
	}
	int size = sizeof(name);
	int retVal = bind(serverSocket, (struct sockaddr *)&name, size);
	if(retVal < 0)
	{
 #ifdef _WINDOWS
		Log::error("Failed. Error Code : %d",WSAGetLastError());
		Log::error("Bind failed %i,%i, %s",retVal,errno, strerror(retVal));
#else
		Log::error("Socked bind failed: %s",strerror(errno));
#endif
		return;
	}
	Log::debug("Bind OK");
	listen(serverSocket, 5);

	fd_set infds;
	FD_ZERO(&infds);
	FD_SET(serverSocket, &infds);

	while(d->running) {
		tv.tv_sec = 5;

		int response = select(serverSocket+1, &infds, NULL, NULL, &tv);
		if (response == 0) {
			FD_SET(serverSocket, &infds);
			continue;
		} else if (response < 0 ) {
			continue;
		}
		// Make sure it is a new connection
		if (!FD_ISSET(serverSocket, &infds)) {
			continue;
		}
		SOCKET_T clientSocket = accept(serverSocket, NULL, NULL);

		ConnectionListenerEventData *data = new ConnectionListenerEventData();
		data->socket = new TelldusCore::Socket(clientSocket);
		d->waitEvent->signal(data);
	}
	SOCKCLOSE(serverSocket);
}

