#include "devicewidget.h"

#include <QApplication>
#include <QEvent>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QMessageBox>
#include <QHeaderView>
#include <QMenu>
#include <QTimer>

#include <QDebug>

#include "telldusgui.h"
#include "editdevicedialog.h"
#include "editgroupdialog.h"
#include "methodwidget.h"

DeviceWidget::DeviceWidget(QWidget *parent) :
	QWidget(parent),
	deviceView(this),
	addToolButton(this),
	removeToolButton(this),
	editToolButton(this)
{
	sortedModel.setSourceModel(&model);
	sortedModel.setDynamicSortFilter(true);
	sortedModel.setSortCaseSensitivity(Qt::CaseInsensitive);
	sortedModel.sort(1, Qt::AscendingOrder);
	deviceView.setModel( &sortedModel );
	deviceView.hideColumn(2);
	deviceView.resizeColumnsToContents();
	deviceView.resizeRowsToContents();
	connect( &deviceView, SIGNAL(clicked(const QModelIndex &)), this, SLOT(listActivated(const QModelIndex &)) );
	connect(&model, SIGNAL(showMessage(const QString &, const QString &, const QString &)), this, SIGNAL(showMessage(const QString &, const QString &, const QString &)));
	connect(&model, SIGNAL(eventTriggered(const QString &, const QString &)), this, SIGNAL(eventTriggered(const QString &, const QString &)));

	QVBoxLayout *layout = new QVBoxLayout(this);
	layout->addWidget(&deviceView);

	QHBoxLayout *buttonLayout = new QHBoxLayout;
	buttonLayout->setSpacing(0);

	QMenu *newMenu = new QMenu( this );
	QAction *newDeviceMenuAction = newMenu->addAction( tr("New device...") );
	connect(newDeviceMenuAction, SIGNAL(triggered()), this, SLOT(addDevice()));
	QAction *newGroupMenuAction = newMenu->addAction( tr("New group...") );
	connect(newGroupMenuAction, SIGNAL(triggered()), this, SLOT(addGroup()));

	addToolButton.setIcon( QIcon( ":/images/list-add.png" ) );
	addToolButton.setText( tr("New") );
	addToolButton.setToolButtonStyle( Qt::ToolButtonTextBesideIcon );
	addToolButton.setPopupMode( QToolButton::MenuButtonPopup );
	addToolButton.setMenu( newMenu );
	connect(&addToolButton, SIGNAL(clicked()), this, SLOT(addDevice()));
	buttonLayout->addWidget( &addToolButton );

	buttonLayout->addSpacing( 10 );

	QMenu *editMenu = new QMenu( this );
	editDeviceMenuAction = editMenu->addAction( tr("Edit device...") );
	connect(editDeviceMenuAction, SIGNAL(triggered()), this, SLOT(editDevice()));
	editGroupMenuAction = editMenu->addAction( tr("Edit group...") );
	connect(editGroupMenuAction, SIGNAL(triggered()), this, SLOT(editGroup()));
	makeGroupMenuAction = editMenu->addAction( tr("Create group...") );
	connect(makeGroupMenuAction, SIGNAL(triggered()), this, SLOT(editGroup()));

	editDeviceMenuAction->setVisible(false);
	editGroupMenuAction->setVisible(false);
	makeGroupMenuAction->setVisible(false);
	editToolButton.setIcon( QIcon( ":/images/list-edit.png" ) );
	editToolButton.setText( tr("Edit") );
	editToolButton.setToolButtonStyle( Qt::ToolButtonTextBesideIcon );
	editToolButton.setPopupMode( QToolButton::MenuButtonPopup );
	editToolButton.setMenu( editMenu );
	editToolButton.setEnabled( false );
	connect(&editToolButton, SIGNAL(clicked()), this, SLOT(editGroupOrDevice()));
	buttonLayout->addWidget( &editToolButton );

	buttonLayout->addSpacing( 10 );

	removeToolButton.setIcon( QIcon( ":/images/list-remove.png" ) );
	removeToolButton.setEnabled( false );
	removeToolButton.setText( tr("Remove") );
	removeToolButton.setToolButtonStyle( Qt::ToolButtonTextBesideIcon );
	connect(&removeToolButton, SIGNAL(clicked()), this, SLOT(deleteDevice()));
	buttonLayout->addWidget( &removeToolButton );

	showStats.setEnabled( true );
	showStats.setChecked( false );
	showStats.setText( tr("Show Stats") );
	connect(&showStats, SIGNAL(toggled(bool)), this, SLOT(showControllerStats(bool)));
	buttonLayout->addWidget( &showStats );

	buttonLayout->addStretch();

	layout->addLayout( buttonLayout );

	if (model.haveError()) {
		//We emit the signal in the next "event loop".
		//This to allow the signals to be connected from our parent object
		QTimer::singleShot(0, this, SLOT(emitError()));
		this->setEnabled( false );
	}

}

DeviceWidget::~DeviceWidget()
{
}

void DeviceWidget::changeEvent(QEvent *e)
{
	switch(e->type()) {
	case QEvent::LanguageChange:
		//m_ui->retranslateUi(this);
		break;
	default:
		break;
	}
}

void DeviceWidget::emitError() {
	emit showMessage("", model.errorString(), "");
}

void DeviceWidget::showControllerStats(bool show)
{
	if(show)
		deviceView.showColumn(2);
	else
		deviceView.hideColumn(2);
}

void DeviceWidget::addDevice() {
	Device device(0, 0, 0);

	EditDeviceDialog *dialog = new EditDeviceDialog(&device);
	if (dialog->exec() == QDialog::Accepted) {
		device.save();
		//Select the new row
// 		QModelIndex index = model.index(model.rowCount()-1, 0, QModelIndex());
// 		deviceView.setCurrentIndex(index);
	}

	delete dialog;
}

void DeviceWidget::addGroup() {
	//Device *device = model.newDevice();
	Device device(0, 0, 0);

	EditGroupDialog *dialog = new EditGroupDialog(&device, &model);
	if (dialog->exec() == QDialog::Accepted) {
		device.save();
	} else {
		//delete device;
	}

	delete dialog;
}

void DeviceWidget::deleteDevice() {
	QMessageBox msgBox;
	msgBox.setText( tr("Are you sure you want to remove the selected device?") );
	msgBox.setInformativeText( tr("The device will be removed permanently from this application as well as all other applications.") );
	msgBox.setIcon( QMessageBox::Warning );
	msgBox.setStandardButtons( QMessageBox::Yes | QMessageBox::No );
	msgBox.setDefaultButton( QMessageBox::No );
	if ( msgBox.exec() ==  QMessageBox::Yes) {
		QModelIndex index = sortedModel.mapToSource(deviceView.currentIndex());
		Device *device = model.device(index);
		if (device) {
			device->remove();
		}
	}
}

void DeviceWidget::editGroupOrDevice() {
	QModelIndex index = sortedModel.mapToSource(deviceView.currentIndex());
	Device device( model.deviceId(index), model.controllerId(index), 0 );

	QDialog *dialog;
	if (device.deviceType() == TELLSTICK_TYPE_GROUP) {
		dialog = new EditGroupDialog( &device, &model );
	} else {
		dialog = new EditDeviceDialog( &device );
	}
	if (dialog->exec() == QDialog::Accepted) {
		device.save();
	}

	delete dialog;
}

void DeviceWidget::editGroup() {
	QModelIndex index = sortedModel.mapToSource(deviceView.currentIndex());
	Device device( model.deviceId(index), model.controllerId(index), 0 );

	QDialog *dialog;
	dialog = new EditGroupDialog( &device, &model );
	if (dialog->exec() == QDialog::Accepted) {
		device.save();
	}

	delete dialog;
}

void DeviceWidget::editDevice() {
	QModelIndex index = sortedModel.mapToSource(deviceView.currentIndex());
	Device device( model.deviceId(index), model.controllerId(index), 0 );
	printf("Edit device: %i\n",device.id());
	QDialog *dialog;
	if (device.deviceType() == TELLSTICK_TYPE_DEVICE || device.deviceType() == TELLSTICK_TYPE_DEVICEGROUP ) {
		dialog = new EditDeviceDialog( &device );
	} else {
		return;
	}
	if (dialog->exec() == QDialog::Accepted) {
		device.save();
	}

	delete dialog;
}

void DeviceWidget::listActivated(const QModelIndex &) {
	QModelIndex index = sortedModel.mapToSource(deviceView.currentIndex());
	Device device( model.deviceId(index), model.controllerId(index), 0 );
	if (device.deviceType() == TELLSTICK_TYPE_GROUP || device.deviceType() == TELLSTICK_TYPE_DEVICEGROUP ) {
		editGroupMenuAction->setVisible(true);
		makeGroupMenuAction->setVisible(false);
	}
	else {
		editGroupMenuAction->setVisible(false);
		makeGroupMenuAction->setVisible(true);
	}
	if (device.deviceType() == TELLSTICK_TYPE_DEVICE || device.deviceType() == TELLSTICK_TYPE_DEVICEGROUP ) {
		editDeviceMenuAction->setVisible(true);
	}
	else {
		editDeviceMenuAction->setVisible(false);
	}
	removeToolButton.setEnabled( true );
	editToolButton.setEnabled( true );
}
