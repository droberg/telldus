#ifndef SENSORSPLUGIN_H
#define SENSORSPLUGIN_H

#include <QScriptExtensionPlugin>

class SensorsPlugin : public QScriptExtensionPlugin {
	Q_OBJECT
	Q_PLUGIN_METADATA(IID "SensorsInterface" FILE "telldussensors.json")
public:
	SensorsPlugin ( QObject * parent = 0 );
	~SensorsPlugin ();

	virtual void initialize ( const QString & key, QScriptEngine * engine );
	virtual QStringList keys () const;
};


#endif // SENSORSPLUGIN_H
