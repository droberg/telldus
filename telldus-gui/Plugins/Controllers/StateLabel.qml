import QtQuick 2.12

Text {
	property int currentState: -1
	property int state: 0
	id: stateLabel;
	text: ""
	font.bold: state == currentState
}
