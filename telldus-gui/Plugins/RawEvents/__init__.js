/** Sensors **/
__setupPackage__( __extension__ );

__postInit__ = function() {
	application.allDoneLoading.connect( com.telldus.rawevents.init );
}

com.telldus.rawevents = function() {
	var view = null;

	function init() {
		view = new com.telldus.qml.view({
		});
		view.setProperty('rawEventsModel', com.telldus.controllers.myobject);
		view.load("main.qml");
		view.sizeRootObjectToView(true);
		application.addWidget("rawevents.gui", 'kde-oxygen-utilities-system-monitor.png', view)
	}
	return { //Public functions
		init:init
	}

}();
