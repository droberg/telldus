#ifndef TELLDUS_CORE_SERVICE_TELLSTICK_H_
#define TELLDUS_CORE_SERVICE_TELLSTICK_H_

#include <list>
#include <string>
#include "service/Controller.h"
#include "common/Thread.h"

class TellStickDescriptor {
public:
	TellStickDescriptor();
	virtual ~TellStickDescriptor();

	std::string serial;
};

class TellStick : public Controller, public TelldusCore::Thread {
public:
	TellStick(int id, TelldusCore::EventRef event, TelldusCore::EventRef updateEvent);

	virtual bool haveRawCapability() const ;
	virtual std::string serial() const;
	virtual bool isOpen() const;
	virtual bool isSameAsDescriptor(const TellStickDescriptor *d) const;
	virtual int reset();
	virtual int send( const std::string &message );
	virtual bool stillConnected() const;
};

#endif // TELLDUS_CORE_SERVICE_TELLSTICK_H_
