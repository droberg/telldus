//
// Copyright (C) 2012 Telldus Technologies AB. All rights reserved.
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "client/Client.h"

#include <list>

#include<stdio.h>
#include<string.h>
#include<errno.h>

#include "common/WinSocketHandler.h"
#include "common/Socket.h"
#include "common/Strings.h"
#include "common/Mutex.h"
#include "client/CallbackDispatcher.h"
#include "client/CallbackMainDispatcher.h"

namespace TelldusCore {

class Client::PrivateData {
public:
	bool running, sensorCached, controllerCached;
	std::wstring sensorCache, controllerCache;
	TelldusCore::Mutex mutex;
	CallbackMainDispatcher callbackMainDispatcher;
	int serverClientPort;
	int serverEventPort;
	std::string serverHostName;
	Socket clientSocket;
	Socket eventSocket;
};

Client *Client::instance = 0;

Client::Client()
	: Thread() {
	d = new PrivateData;
	d->running = false;
	d->sensorCached = false;
	d->controllerCached = false;
	d->serverClientPort = TELLSTICK_DEFAULT_CLIENT_PORT;
	d->serverEventPort = TELLSTICK_DEFAULT_EVENT_PORT;
	d->serverHostName = std::string(TELLSTICK_DEFAULT_IPADDRESS);
}

Client::Client(std::string serverAddress, int clientPort, int eventPort)
	: Thread() {
	d = new PrivateData;
	d->running = false;
	d->sensorCached = false;
	d->controllerCached = false;
	d->serverClientPort = clientPort;
	d->serverEventPort = eventPort;
	d->serverHostName = serverAddress;
}

Client::~Client(void) {
	stopThread();
	wait();
	{
		TelldusCore::MutexLocker locker(&d->mutex);
	}
	delete d;
}

void Client::close() {
	if (Client::instance != 0) {
		delete Client::instance;
		Client::instance = 0;
	}
}

Client *Client::getInstance(bool runInstance) {
	if (Client::instance == 0) {
		Client::instance = new Client();
	}
	if(runInstance && ! instance->d->running)
	{
		instance->d->running = true;
		instance->start();
	}
	return Client::instance;
}

Client *Client::getInstance(std::string serverAddress, int clientPort, int eventPort, bool runInstance) {
	if (Client::instance == 0) {
		Client::instance = new Client(serverAddress, clientPort, eventPort);
	}
	if(runInstance && ! instance->d->running)
	{
		instance->d->running = true;
		instance->start();
	}
	return Client::instance;
}

bool Client::getBoolFromService(const Message &msg) {
	return getIntegerFromService(msg) == TELLSTICK_SUCCESS;
}

int Client::getIntegerFromService(const Message &msg) {
	std::wstring response = sendToService(msg);
	if (response.compare(L"") == 0) {
		return TELLSTICK_ERROR_COMMUNICATING_SERVICE;
	}
	return Message::takeInt(&response);
}

std::wstring Client::getWStringFromService(const Message &msg) {
	std::wstring response = sendToService(msg);
	return Message::takeString(&response);
}

int Client::getServerIpFromHostName(sockaddr_in *remoteServerAddr, std::string hostName, int port)
{
	initSocketsIfNecessary();
	struct addrinfo hints, *res;
	int errcode;
	char tmpAddrStr[100];
	char finalAddrStr[100];
	void *ptr;
	memset (&hints, 0, sizeof (hints));
	hints.ai_family = PF_UNSPEC;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_flags |= AI_CANONNAME;
	errcode = getaddrinfo (hostName.c_str(), NULL, &hints, &res);
	if (errcode != 0)
	{
		printf("Error: %s: %s\n", gai_strerror(errcode),hostName.c_str());
		return -1;
	}

	//printf ("Host from getServerIpFromHostName %s\n", hostName.c_str());
	while (res)
	{
		inet_ntop (res->ai_family, res->ai_addr->sa_data, tmpAddrStr, sizeof(tmpAddrStr));
		switch (res->ai_family)
		{
			case AF_INET:
				ptr = &((struct sockaddr_in *) res->ai_addr)->sin_addr;
				inet_ntop (res->ai_family, ptr, finalAddrStr, sizeof(finalAddrStr));
				break;
			case AF_INET6:
				ptr = &((struct sockaddr_in6 *) res->ai_addr)->sin6_addr;
				break;
		}
		res = res->ai_next;
	}
	//printf ("Final addr from getServerIpFromHostName %s / port %i\n",finalAddrStr,port);

	remoteServerAddr->sin_family = AF_INET;
	if(inet_pton(AF_INET, finalAddrStr, &(remoteServerAddr->sin_addr))<=0)
	{
		printf("Could not convert ip address to number\n");
		return -1;
	}
	remoteServerAddr->sin_port = htons( (uint16_t) port);

	return 0;
}

int Client::registerEvent( CallbackStruct::CallbackType type, void *eventFunction, void *context ) {
	return d->callbackMainDispatcher.registerCallback(type, eventFunction, context );
}

void Client::run() {
	sockaddr_in remoteServerAddr;
	memset(&remoteServerAddr, 0, sizeof(remoteServerAddr));
	if( getServerIpFromHostName(&remoteServerAddr, d->serverHostName, d->serverEventPort) != 0)
		return;
	d->eventSocket.connect(&remoteServerAddr);

	while(d->running) {
		if(!d->eventSocket.isConnected()) {
			d->eventSocket.connect(&remoteServerAddr);  // try to reconnect to service
			if(!d->eventSocket.isConnected()) {
				// reconnect didn't succeed, wait a while and try again
				msleep(100);
				continue;
			}
		}

		std::wstring clientMessage = d->eventSocket.read(1000);  // testing 5 second timeout

		while(clientMessage != L"") {
			// a message arrived
			std::wstring type = Message::takeString(&clientMessage);
			if(type == L"TDDeviceChangeEvent") {
				DeviceChangeEventCallbackData *data = new DeviceChangeEventCallbackData();
				data->deviceId = Message::takeInt(&clientMessage);
				data->changeEvent = Message::takeInt(&clientMessage);
				data->changeType = Message::takeInt(&clientMessage);
				d->callbackMainDispatcher.execute(CallbackStruct::DeviceChangeEvent, data);

			} else if(type == L"TDDeviceEvent") {
				DeviceEventCallbackData *data = new DeviceEventCallbackData();
				data->deviceId = Message::takeInt(&clientMessage);
				data->deviceState = Message::takeInt(&clientMessage);
				data->deviceStateValue = TelldusCore::wideToString(Message::takeString(&clientMessage));
				d->callbackMainDispatcher.execute(CallbackStruct::DeviceEvent, data);

			} else if(type == L"TDRawDeviceEvent") {
				RawDeviceEventCallbackData *data = new RawDeviceEventCallbackData();
				data->data = TelldusCore::wideToString(Message::takeString(&clientMessage));
				data->controllerId = Message::takeInt(&clientMessage);
				d->callbackMainDispatcher.execute(CallbackStruct::RawDeviceEvent, data);

			} else if(type == L"TDSensorEvent") {
				SensorEventCallbackData *data = new SensorEventCallbackData();
				data->protocol = TelldusCore::wideToString(Message::takeString(&clientMessage));
				data->model = TelldusCore::wideToString(Message::takeString(&clientMessage));
				data->name = TelldusCore::wideToString(Message::takeString(&clientMessage));
				data->id = Message::takeInt(&clientMessage);
				data->dataType = Message::takeInt(&clientMessage);
				data->value = TelldusCore::wideToString(Message::takeString(&clientMessage));
				data->timestamp = Message::takeInt(&clientMessage);
				d->callbackMainDispatcher.execute(CallbackStruct::SensorEvent, data);

			} else if(type == L"TDSensorChangeEvent") {
				SensorChangeEventCallbackData *data = new SensorChangeEventCallbackData();
				data->sensorId = Message::takeInt(&clientMessage);
				data->changeEvent = Message::takeInt(&clientMessage);
				data->changeType = Message::takeInt(&clientMessage);
				d->callbackMainDispatcher.execute(CallbackStruct::SensorChangeEvent, data);

			} else if(type == L"TDControllerEvent") {
				ControllerEventCallbackData *data = new ControllerEventCallbackData();
				data->controllerId = Message::takeInt(&clientMessage);
				data->changeEvent = Message::takeInt(&clientMessage);
				data->changeType = Message::takeInt(&clientMessage);
				data->newValue = TelldusCore::wideToString(Message::takeString(&clientMessage));
				d->callbackMainDispatcher.execute(CallbackStruct::ControllerEvent, data);

			} else if(type == L"TDSettingsChangeEvent") {
				SettingsChangeEventCallbackData *data = new SettingsChangeEventCallbackData();
				data->changeType = Message::takeInt(&clientMessage);
				d->callbackMainDispatcher.execute(CallbackStruct::SettingsEvent, data);
			} else {
				clientMessage = L"";  // cleanup, if message contained garbage/unhandled data
			}
		}
	}
}

std::wstring Client::sendToService(const Message &msg) {
	Client* instance = getInstance(false);
	return instance->internalSendToService(msg);
}

std::wstring Client::internalSendToService(const Message &msg)
{
	int tries = 0;
	std::wstring readData;
	while(tries < 4) {
		tries++;
		if(tries == 4) {
			TelldusCore::Message msg;
			msg.addArgument(TELLSTICK_ERROR_CONNECTING_SERVICE);
			return msg;
		}
		if( ! d->clientSocket.isConnected() )
		{
			sockaddr_in remoteServerAddr;
			memset(&remoteServerAddr, 0, sizeof(remoteServerAddr));
			if( getServerIpFromHostName(&remoteServerAddr, d->serverHostName, d->serverClientPort) != 0)
				return L"";
			//printf("Client::sendToService, connect attempt... (IP/port = %s,%i)\n",inet_ntoa(remoteServerAddr.sin_addr),ntohs(remoteServerAddr.sin_port));
			d->clientSocket.connect(&remoteServerAddr);
		}
		if (!d->clientSocket.isConnected()) {  // sConnection failed
			continue;  // retry
		}
		//readData = d->clientSocket.read(1);
		d->clientSocket.write(msg.data());
		if (!d->clientSocket.isConnected()) {  // Connection failed sometime during operation... (better check here, instead of 5 seconds timeout later)
			msleep(100);
			continue;  // retry
		}
		readData = d->clientSocket.read(1000);
		if(readData == L"") {
			continue;
		}
		if (!d->clientSocket.isConnected()) {  // Connection failed sometime during operation...
			continue;  // retry
		}
		break;
	}

	return readData;
}

void Client::stopThread() {
	d->running = false;
	d->eventSocket.stopReadWait();
}

int Client::unregisterCallback( int callbackId ) {
	return d->callbackMainDispatcher.unregisterCallback(callbackId);
}

int Client::getSensor(char *protocol, int protocolLen, char *model, int modelLen, char* name, int nameLen, int *sensorId, int *dataTypes) {
	if (!d->sensorCached) {
		Message msg(L"tdSensor");
		std::wstring response = Client::getWStringFromService(msg);
		int count = Message::takeInt(&response);
		d->sensorCached = true;
		d->sensorCache = L"";
		if (count > 0) {
			d->sensorCache = response;
		}
	}

	if (d->sensorCache == L"") {
		d->sensorCached = false;
		return TELLSTICK_ERROR_DEVICE_NOT_FOUND;
	}

	std::wstring p = Message::takeString(&d->sensorCache);
	std::wstring m = Message::takeString(&d->sensorCache);
	std::wstring n = Message::takeString(&d->sensorCache);
	int id = Message::takeInt(&d->sensorCache);
	int dt = Message::takeInt(&d->sensorCache);

	if (protocol && protocolLen) {
		strncpy(protocol, TelldusCore::wideToString(p).c_str(), protocolLen);
	}
	if (model && modelLen) {
		strncpy(model, TelldusCore::wideToString(m).c_str(), modelLen);
	}
	if (name && nameLen) {
		strncpy(name, TelldusCore::wideToString(n).c_str(), nameLen);
	}
	if (sensorId) {
		(*sensorId) = id;
	}
	if (dataTypes) {
		(*dataTypes) = dt;
	}

	return TELLSTICK_SUCCESS;
}

int Client::getController(int *controllerId, int *controllerType, char *name, int nameLen, int *available) {
	if (!d->controllerCached) {
		Message msg(L"tdController");
		std::wstring response = Client::getWStringFromService(msg);
		int count = Message::takeInt(&response);
		d->controllerCached = true;
		d->controllerCache = L"";
		if (count > 0) {
			d->controllerCache = response;
		}
	}

	if (d->controllerCache == L"") {
		d->controllerCached = false;
		return TELLSTICK_ERROR_NOT_FOUND;
	}

	int id = Message::takeInt(&d->controllerCache);
	int type = Message::takeInt(&d->controllerCache);
	std::wstring n = Message::takeString(&d->controllerCache);
	int a = Message::takeInt(&d->controllerCache);

	if (controllerId) {
		(*controllerId) = id;
	}
	if (controllerType) {
		(*controllerType) = type;
	}
	if (name && nameLen) {
		strncpy(name, TelldusCore::wideToString(n).c_str(), nameLen);
	}
	if (available) {
		(*available) = a;
	}

	return TELLSTICK_SUCCESS;
}

}  // namespace TelldusCore
