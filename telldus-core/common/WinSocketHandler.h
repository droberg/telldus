#ifndef WINSOCKETHANDLER_H
#define WINSOCKETHANDLER_H
// TCP/IP
#ifdef _WINDOWS
#include <winsock2.h>
#include <Ws2tcpip.h>
#include <windows.h>
#pragma comment(lib,"ws2_32.lib") //Winsock Library
#define SOCKCLOSE closesocket
typedef char setsockoptType;
#else
#define NO_ERROR ( 0 )
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <netdb.h>
#define SOCKCLOSE close
typedef int setsockoptType;
#endif

void initSocketsIfNecessary();
void cleanupSocketsIfNecessary();

#endif // WINSOCKETHANDLER_H
