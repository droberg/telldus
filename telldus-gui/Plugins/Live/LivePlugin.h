#ifndef LIVEPLUGIN_H
#define LIVEPLUGIN_H

#include <QScriptExtensionPlugin>

class LivePlugin : public QScriptExtensionPlugin {
	Q_OBJECT
	Q_PLUGIN_METADATA(IID "LiveInterface" FILE "tellduslive.json")

public:
	LivePlugin ( QObject * parent = 0 );
	~LivePlugin ();

	virtual void initialize ( const QString & key, QScriptEngine * engine );
	virtual QStringList keys () const;
};


#endif // LIVEPLUGIN_H
