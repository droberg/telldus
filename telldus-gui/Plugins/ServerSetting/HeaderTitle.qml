import QtQuick 2.12

Text {
	id: headerTitle
	text: "Name"
	color: "white"
	font.weight: Font.Bold
	height: parent.height
	verticalAlignment: Text.AlignVCenter
}
