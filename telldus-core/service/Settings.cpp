//
// Copyright (C) 2012 Telldus Technologies AB. All rights reserved.
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "service/Settings.h"
#include <string>
#include "common/Strings.h"
TelldusCore::Mutex Settings::mutex;
Settings* Settings::instance = nullptr;
int Settings::numSettingsUsers = 0;


Settings* Settings::getInstance(void)
{
	if (instance == nullptr)
	{
		instance = new Settings();
	}
	numSettingsUsers += 1;
	return instance;
}

void Settings::deleteInstance(void)
{
	numSettingsUsers -= 1;
	if(numSettingsUsers <= 0)
	{
		numSettingsUsers = 0;
		if( instance != nullptr )
			delete instance;
		instance = nullptr;
	}
}
/*
* Get the name of the device
*/
std::wstring Settings::getName(Node type, int intNodeId) const {
	TelldusCore::MutexLocker locker(&mutex);
	return getStringSetting(type, intNodeId, L"name", false);
}

/*
* Set the name of the device
*/
int Settings::setName(Node type, int intDeviceId, const std::wstring &strNewName) {
	TelldusCore::MutexLocker locker(&mutex);
	return setStringSetting(type, intDeviceId, L"name", strNewName, false);
}

/*
* Get the device vendor
*/
std::wstring Settings::getProtocol( Settings::Node type, int intDeviceId) const {
	TelldusCore::MutexLocker locker(&mutex);
	return getStringSetting(type, intDeviceId, L"protocol", false);
}

/*
* Set the device vendor
*/
int Settings::setProtocol(Settings::Node type, int intDeviceId, const std::wstring &strVendor) {
	TelldusCore::MutexLocker locker(&mutex);
	return setStringSetting(type, intDeviceId, L"protocol", strVendor, false);
}

/*
* Get the device model
*/
std::wstring Settings::getModel(Settings::Node type,int intDeviceId) const {
	TelldusCore::MutexLocker locker(&mutex);
	return getStringSetting(type, intDeviceId, L"model", false);
}

/*
* Set the device model
*/
int Settings::setModel(Node type, int intDeviceId, const std::wstring &strModel) {
	TelldusCore::MutexLocker locker(&mutex);
	return setStringSetting(type, intDeviceId, L"model", strModel, false);
}

/*
* Get the device getUnknownAtStart setting
*/
int Settings::getUnknownAtStart(int intDeviceId) const
{
	TelldusCore::MutexLocker locker(&mutex);
	return getIntSetting(Device, intDeviceId, L"unknownatstart", false);
}

/*
* Set the device getUnknownAtStart setting
*/
int Settings::setUnknownAtStart(int intDeviceId, int unknownAtStart)
{
	TelldusCore::MutexLocker locker(&mutex);
	return setIntSetting(Device, intDeviceId, L"unknownatstart", unknownAtStart, false);
}

/*
* Set device argument
*/
int Settings::setDeviceParameter(int intDeviceId, const std::wstring &strName, const std::wstring &strValue) {
	TelldusCore::MutexLocker locker(&mutex);
	return setStringSetting(Device, intDeviceId, strName, strValue, true);
}

/*
* Get device argument
*/
std::wstring Settings::getDeviceParameter(int intDeviceId, const std::wstring &strName) const {
	TelldusCore::MutexLocker locker(&mutex);
	return getStringSetting(Device, intDeviceId, strName, true);
}

/*
* Set preferred controller id
*/
int Settings::setPreferredControllerId(int intDeviceId, int value) {
	TelldusCore::MutexLocker locker(&mutex);
	return setIntSetting(Device, intDeviceId,  L"controller", value, false);
}

std::wstring Settings::getMaxValues( int intNodeId)
{
	TelldusCore::MutexLocker locker(&mutex);
	std::wstring stringSetting = getStringSetting(Settings::Sensor, intNodeId, L"maxvalues", false);
	if ( stringSetting == L"" )
	{
		stringSetting = L"temp:100;humidity:100;rainrate:;raintotal:;winddirecetion:;windaverage:;windgust:;";
		setStringSetting(Settings::Sensor, intNodeId, L"maxvalues", stringSetting, false);
	}
	return stringSetting;
}

std::wstring Settings::getMinValues(int intNodeId)
{
	TelldusCore::MutexLocker locker(&mutex);
	std::wstring stringSetting = getStringSetting(Settings::Sensor, intNodeId, L"minvalues", false);;
	if ( stringSetting == L"" )
	{
		stringSetting = L"temp:-50;humidity:0;rainrate:;raintotal:;winddirecetion:;windaverage:;windgust:;";
		setStringSetting(Settings::Sensor, intNodeId, L"minvalues", stringSetting, false);
	}
	return stringSetting;
}

std::wstring Settings::getMaxRates(int intNodeId)
{
	TelldusCore::MutexLocker locker(&mutex);
	std::wstring stringSetting = getStringSetting(Settings::Sensor, intNodeId, L"maxrates", false);
	if ( stringSetting == L"" )
	{
		stringSetting = L"temp:1;humidity:1;rainrate:;raintotal:;winddirecetion:;windaverage:;windgust:;";
		setStringSetting(Settings::Sensor, intNodeId, L"maxrates", stringSetting, false);
	}
	return stringSetting;
}

int Settings::getNOutlisersBeforeAccept(int intNodeId)
{
	TelldusCore::MutexLocker locker(&mutex);
	int intSetting = 3;
#ifndef _CONFUSE
	std::wstring strSetting = getStringSetting(Settings::Sensor, intNodeId, L"noutliersbeforeaccept", false);
	if (strSetting.length()) {
		intSetting = static_cast<int>(strSetting[0]);
	}
	else
		setIntSetting(Settings::Sensor, intNodeId, L"noutliersbeforeaccept", intSetting, false);
#else
	intSetting = getIntSetting(Settings::Sensor, intNodeId, L"noutliersbeforeaccept", false);
#endif
	return intSetting;
}

/*
* Get preferred controller id
*/
int Settings::getPreferredControllerId(int intDeviceId) {
	TelldusCore::MutexLocker locker(&mutex);
	return getIntSetting(Device, intDeviceId, L"controller", false);
}

std::wstring Settings::getControllerSerial(int intControllerId) const {
	TelldusCore::MutexLocker locker(&mutex);
	return getStringSetting(Controller, intControllerId, L"serial", false);
}

int Settings::setControllerSerial(int intControllerId, const std::wstring &serial) {
	TelldusCore::MutexLocker locker(&mutex);
	return setStringSetting(Controller, intControllerId,  L"serial", serial, false);
}

int Settings::getControllerType(int intControllerId) const {
	TelldusCore::MutexLocker locker(&mutex);
	return getIntSetting(Controller, intControllerId, L"type", false);
}

int Settings::setControllerType(int intControllerId, int type) {
	TelldusCore::MutexLocker locker(&mutex);
	return setIntSetting(Controller, intControllerId,  L"type", type, false);
}

int Settings::getIgnoreController(int intControllerId) const
{
	TelldusCore::MutexLocker locker(&mutex);
	return getIntSetting(Controller, intControllerId, L"ignore", false);
}

int Settings::setIgnoreController(int intControllerId, int type)
{
	TelldusCore::MutexLocker locker(&mutex);
	return setIntSetting(Controller, intControllerId,  L"ignore", type, false);
}

std::string Settings::getNodeString(Settings::Node type) const {
	if (type == Device) {
		return "device";
	} else if (type == Controller) {
		return "controller";
	} else if (type == Sensor ) {
		return "sensor";
	}
	return "";
}

#ifndef _CONFUSE

bool Settings::setDeviceState( int intDeviceId, int intDeviceState, const std::wstring &strDeviceStateValue, const std::string &controllerStats) {
	TelldusCore::MutexLocker locker(&mutex);
	bool retval = setIntSetting( Settings::Device, intDeviceId, L"state", intDeviceState, true );
	setStringSetting( Settings::Device, intDeviceId, L"stateValue", strDeviceStateValue, true );
	return retval;
}

int Settings::getDeviceState( int intDeviceId ) const {
	TelldusCore::MutexLocker locker(&mutex);
	return getIntSetting( Settings::Device, intDeviceId, L"state", true );
}

std::wstring Settings::getDeviceStateValue( int intDeviceId ) const {
	TelldusCore::MutexLocker locker(&mutex);
	return getStringSetting( Settings::Device, intDeviceId, L"stateValue", true );
}

std::string Settings::getControllerStats(int intDeviceId) const
{
	TelldusCore::MutexLocker locker(&mutex);
	return TelldusCore::wideToString(getStringSetting( Settings::Device, intDeviceId, L"controllerStats", true ));
}

bool Settings::setSensorState( int intDeviceId,  const std::wstring & timestamp, const std::wstring &state) {
	TelldusCore::MutexLocker locker(&mutex);
	bool retval = setStringSetting( Settings::Sensor, intDeviceId, L"timestamp", timestamp, true );
	setStringSetting( Settings::Sensor, intDeviceId, L"state", state, true );
	return retval;
}

std::wstring  Settings::getStateValueMap( int intDeviceId ) const {
	TelldusCore::MutexLocker locker(&mutex);
	return getStringSetting(Settings::Sensor, intDeviceId, L"state", false);
}

std::wstring Settings::getSensorTimestamp( int intDeviceId ) const {
	TelldusCore::MutexLocker locker(&mutex);
	return getStringSetting(Settings::Sensor, intDeviceId, L"timestamp", false);
}
#endif
