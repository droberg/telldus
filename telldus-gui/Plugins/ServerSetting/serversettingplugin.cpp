#include "serversettingplugin.h"
#include <QScriptEngine>
#include <QQuickView>
#include <QSettings>

ServersettingPlugin::ServersettingPlugin ( QObject * parent )
    :QScriptExtensionPlugin( parent )
{
}

ServersettingPlugin::~ServersettingPlugin() {

}

void ServersettingPlugin::initialize ( const QString & key, QScriptEngine * engine ) {
	if (key == "com.telldus.serversetting") {
		qmlRegisterType<MyObject>("Telldus", 1, 0, "MyObject");

		QScriptValue qml = engine->globalObject().property("com").property("telldus").property("serversetting");
	}
}

QStringList ServersettingPlugin::keys () const {
	return QStringList() << "com.telldus.serversetting";
}
