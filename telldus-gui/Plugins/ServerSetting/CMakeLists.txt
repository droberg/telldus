SET(REQUIRE_PLUGIN_QML TRUE PARENT_SCOPE)
SET(REQUIRE_PLUGIN_SETTINGS TRUE PARENT_SCOPE)
SET( Plugin_NAME "Serversetting" )

SET( Plugin_SRCS
	serversettingplugin.cpp
)

SET( Plugin_HDRS
	serversettingplugin.h
)

SET( Plugin_MOC_HDR
)

SET( Plugin_RESOURCES
	serversetting.qrc
)

SET( Plugin_PATH "com.telldus.serversetting" )

SET( Plugin_EXTRA
	HeaderTitle.qml
	images/TelldusCenter_128.png
	images/devices-bw.png
	images/devices.png
	images/header_bg.png
	images/kde-oxygen-network-server.png
	images/row_bg.png
	images/tellstick.png
	images/tellstick_duo.png
	main.qml
	qmldir
	telldusserversetting.json
)

INCLUDE( ../TelldusCenterPlugin.cmake NO_POLICY_SCOPE )
