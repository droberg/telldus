#ifndef CONTROLLERSPLUGIN_H
#define CONTROLLERSPLUGIN_H

#include <QScriptExtensionPlugin>

class ControllersPlugin : public QScriptExtensionPlugin {
	Q_OBJECT
	Q_PLUGIN_METADATA(IID "ControllersInterface" FILE "tellduscontrollers.json")
public:
	ControllersPlugin ( QObject * parent = 0 );
	~ControllersPlugin ();

	virtual void initialize ( const QString & key, QScriptEngine * engine );
	virtual QStringList keys () const;
};


#endif // CONTROLLERSPLUGIN_H
