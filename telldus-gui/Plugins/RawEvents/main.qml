import QtQuick 2.12
import QtQuick.Controls 2.12
import Telldus 1.0
Item {
	width: 500 //Minimum width
	
	Column {
		id: column
		spacing: 1
		anchors.fill: parent

		BorderImage {
			id: header
			source: "header_bg.png"
			width: parent.width; height: 40
			border.left: 5; border.top: 5
			border.right: 5; border.bottom: 5
			HeaderTitle {
				text: "Raw Events"
				anchors.left: parent.left
				anchors.leftMargin: 15
			}
		}
		BorderImage {
			id: view
			source: "row_bg.png"
			border.left: 5; border.top: 5
			border.right: 5; border.bottom: 5
			width: parent.width
			height: parent.height-85
			ScrollView {
				id: scrollview
				clip: true
				width: parent.width
				height: view.height
				leftPadding: 5
				rightPadding: 5
				RawDataObject{
				id: myRawdataobj
				}
				TextArea {
					selectionColor : "#aa0000"
					readOnly: true
					selectByMouse: true
					textFormat: TextEdit.RichText
					text: myRawdataobj.theText
					width: parent.width
					height: view.height-10
					anchors.fill: parent
				}
			}
		}
		BorderImage {
			id: footer
			source: "row_bg.png"
			border.left: 5; border.top: 5
			border.right: 5; border.bottom: 5
			width: parent.width
			height: 40
			Row {
				spacing: 8
				height: 32
				anchors{
					//horizontalCenter: parent.horizontalCenter
					verticalCenter: parent.verticalCenter
				}
				Rectangle {
					width: 4
					height: 32
					opacity: 0
				}
				Image {
					verticalAlignment: Image.AlignVCenter
					source: "kde-oxygen-media-playback-start.png"
					width: 32
					height: 32
					scale:  mouseAreaStart.containsMouse ? 0.8 : 1.0
					smooth: true
					MouseArea {
						id: mouseAreaStart
						anchors.fill: parent
						anchors.margins: -10
						hoverEnabled: true
						onClicked: myRawdataobj.start()
					}
				}
				Image {
					verticalAlignment: Image.AlignVCenter
					source: "kde-oxygen-media-playback-stop.png"
					width: 32
					height: 32
					scale:  mouseAreaStop.containsMouse ? 0.8 : 1.0
					smooth: true
					MouseArea {
						id: mouseAreaStop
						anchors.fill: parent
						anchors.margins: -10
						hoverEnabled: true
						onClicked: myRawdataobj.stop()
					}
				}
				Image {
					verticalAlignment: Image.AlignVCenter
					source: "kde-oxygen-edit-clear.png"
					width: 32
					height: 32
					scale:  mouseAreaClear.containsMouse ? 0.8 : 1.0
					smooth: true
					MouseArea {
						id: mouseAreaClear
						anchors.fill: parent
						anchors.margins: -10
						hoverEnabled: true
						onClicked: myRawdataobj.clearText()
					}
				}
			}
		}
	}
}

/*##^##
Designer {
    D{i:0;formeditorZoom:0.75}
}
##^##*/
