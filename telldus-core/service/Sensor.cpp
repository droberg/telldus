//
// Copyright (C) 2012 Telldus Technologies AB. All rights reserved.
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "stdlib.h"
#include "service/Sensor.h"
#include <cmath>
#include <map>
#include <string>
#include <sstream>
#include "common/common.h"
#include "client/telldus-core.h"

class Sensor::PrivateData {
public:
	// Settings / definitions
	std::wstring protocol, model;
	int id;
	std::wstring name;
	std::map<int, std::string> maxValues;
	std::map<int, std::string> minValues;
	std::map<int, std::string> maxRates;
	int nOutliersBeforeAcceptSetting;
	// State
	std::map<int, std::string> values;
	std::map<int,time_t> timestamp;
	std::map<int, int> nOutliersRecieved; // Init to max
};

Sensor::Sensor(const std::wstring &protocol, const std::wstring &model, int id)
	:Mutex() {
	d = new PrivateData;
	d->protocol = protocol;
	d->model = model;
	d->id = id;
	d->name = L"";
}

Sensor::~Sensor() {
	delete d;
}

std::wstring Sensor::protocol() const {
	return d->protocol;
}

std::wstring Sensor::model() const {
	return d->model;
}

int Sensor::id() const {
	return d->id;
}

time_t Sensor::timestamp(int type) const {
	std::map<int, time_t>::const_iterator it = d->timestamp.find(type);
	if (it == d->timestamp.end()) {
		return 0;
	}
	return (*it).second;
}

std::wstring Sensor::timestampStr() const
{
	std::stringstream ss;
	std::map<int, std::string>::iterator it;
	for (std::map<int, time_t>::iterator it = d->timestamp.begin(); it != d->timestamp.end(); ++it) {
		ss << it->first
		   << ':'
		   << it->second
			   << ';';
	}
	return TelldusCore::charToWstring(ss.str().c_str());
}

std::wstring Sensor::name() const
{
	return d->name;
}

int Sensor::dataTypes() const {
	int retval = 0;
	for (std::map<int, std::string>::iterator it = d->values.begin(); it != d->values.end(); ++it) {
		retval |= (*it).first;
	}
	return retval;
}

void Sensor::setName(std::wstring name)
{
	d->name = name;
}

bool Sensor::setValue(int type, const std::string &newValue, time_t timestamp) {
	std::string strValue;
	if (newValue.substr(0, 2).compare("0x") == 0) {
		int intval = strtol(newValue.c_str(), NULL, 16);
		strValue= TelldusCore::intToString(intval);
	} else {
		strValue = newValue;
	}
	std::string maxValue = d->maxValues[type];
	std::string minValue = d->minValues[type];
	std::string maxRate = d->maxRates[type];
	if( maxValue != "" )
	{
		try {
			float floatMaxVal = std::stof( maxValue );
			float floatNewValue = std::stof( newValue );
			if( floatNewValue > floatMaxVal )
				return false;
		} catch (const std::invalid_argument& ia) { return false; }
	}
	if( minValue != "" )
	{
		try {
			float floatMinVal = std::stof( minValue );
			float floatNewValue = std::stof( newValue );
			if( floatNewValue < floatMinVal )
				return false;
		} catch (const std::invalid_argument& ia) { return false; }
	}
	if( maxRate != "" )
	{
		float floatOldValue = -1.0e10;
		try {
			floatOldValue = std::stof( d->values[type] );
		} catch ( const std::invalid_argument& ia ) {}
		try {
			float floatMaxRate = std::stof( maxRate );
			float floatNewValue = std::stof( newValue );
			int64_t tDiff = timestamp;
			if (d->timestamp.find(type) != d->timestamp.end()) {
				tDiff -= d->timestamp[type];
			}
			if( fabs( floatNewValue - floatOldValue ) > tDiff * floatMaxRate ){
				d->nOutliersRecieved[type] += 1;
				if( d->nOutliersRecieved[type] < d->nOutliersBeforeAcceptSetting ) {
					return false;
				}
				else
					d->nOutliersRecieved[type] = 0;
			}
		} catch ( const std::invalid_argument& ia ) { return false; }
	}
	d->values[type] = strValue;
	d->timestamp[type] = timestamp;
	return true;
}

std::string Sensor::value(int type) const {
	std::map<int, std::string>::const_iterator it = d->values.find(type);
	if (it == d->values.end()) {
		return "";
	}
	return (*it).second;
}

void Sensor::setStateValueMap(std::wstring stateValueString, std::wstring timeStamp)
{
	d->timestamp.clear();
	d->values.clear();
	std::string localStr = TelldusCore::wideToString( stateValueString );
	size_t pos = 0;
	std::string token;
	while ((pos = localStr.find(";")) != std::string::npos) {
		token = localStr.substr(0, pos);
		size_t ipos =token.find(":");
		int type = std::stoi(token.substr(0, ipos),nullptr);
		std::string value = token.erase(0, ipos + 1);
		d->values[type] = value;
		localStr.erase(0, pos + 1);
	}

	localStr = TelldusCore::wideToString( timeStamp );
	pos = 0;
	while ((pos = localStr.find(";")) != std::string::npos) {
		token = localStr.substr(0, pos);
		size_t ipos =token.find(":");
		int type = std::stoi(token.substr(0, ipos),nullptr);
		std::string value = token.erase(0, ipos + 1);
		d->timestamp[type] = std::stoi(value);
		localStr.erase(0, pos + 1);
	}
	return;
}

std::wstring Sensor::stateValueMap() const
{
	std::stringstream ss;
	std::map<int, std::string>::iterator it;
	for (it = d->values.begin(); it !=  d->values.end(); it++)
	{
		ss << it->first
		   << ':'
		   << it->second
		   << ';';
	}
	return TelldusCore::charToWstring(ss.str().c_str());
}

void Sensor::setMaxValues(std::wstring maxValues)
{
	d->maxValues.clear();
	std::string localStr = TelldusCore::wideToString( maxValues );
	size_t pos = 0;
	std::string token;
	while ((pos = localStr.find(";")) != std::string::npos) {
		token = localStr.substr(0, pos);
		size_t ipos =token.find(":");
		std::string type = token.substr(0, ipos);
		std::string value = token.erase(0, ipos + 1);
		d->maxValues[stringToType(type)] = value;
		localStr.erase(0, pos + 1);
	}
	if(localStr.length() > 0)
		token = localStr.substr(0, pos);
	return;
}

void Sensor::setMinValues(std::wstring minValues)
{
	d->minValues.clear();
	std::string localStr = TelldusCore::wideToString( minValues );
	size_t pos = 0;
	std::string token;
	while ((pos = localStr.find(";")) != std::string::npos) {
		token = localStr.substr(0, pos);
		size_t ipos =token.find(":");
		std::string type = token.substr(0, ipos);
		std::string value = token.erase(0, ipos + 1);
		d->minValues[stringToType(type)] = value;
		localStr.erase(0, pos + 1);
	}
	if(localStr.length() > 0)
		token = localStr.substr(0, pos);
	return;
}

void Sensor::setMaxRates(std::wstring maxRate)
{
	d->maxRates.clear();
	std::string localStr = TelldusCore::wideToString( maxRate );
	size_t pos = 0;
	std::string token;
	while ((pos = localStr.find(";")) != std::string::npos) {
		token = localStr.substr(0, pos);
		size_t ipos =token.find(":");
		std::string type = token.substr(0, ipos);
		std::string value = token.erase(0, ipos + 1);
		d->maxRates[stringToType(type)] = value;
		localStr.erase(0, pos + 1);
	}
	if(localStr.length() > 0)
		token = localStr.substr(0, pos);
	return;
}

void Sensor::setNOutlisersBeforeAccept(int nOutliers)
{
	d->nOutliersBeforeAcceptSetting = nOutliers;
	d->nOutliersRecieved.clear();
}

int Sensor::stringToType(std::string type)
{
	int intType = 0;
	if(type == "temp") {
		intType = TELLSTICK_TEMPERATURE;
	} else if(type == "humidity") {
		intType = TELLSTICK_HUMIDITY;
	} else if(type == "rainrate") {
		intType = TELLSTICK_RAINRATE;
	} else if(type == "raintotal") {
		intType = TELLSTICK_RAINTOTAL;
	} else if(type == "winddirecetion") {
		intType = TELLSTICK_WINDDIRECTION;
	} else if(type == "windaverage") {
		intType = TELLSTICK_WINDAVERAGE;
	} else if(type == "windgust") {
		intType = TELLSTICK_WINDGUST;
	}
	return intType;
}
